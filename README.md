# Wikidata Service
A service to query Wikidata for items and their properties. It consists of two components:

- The [Wikidata REST Component](./cmd/wikidata) is a REST service that returns Wikidata items and their properties.
- The [MARC21 Component](./cmd/marc21) is a service that accepts MARC21 records, checks them for GND identifiers,
queries Lobid GND for the corresponding Wikidata ID and then queries the Wikidata REST Component for the item and
its properties. Then returns a generic structure with the data.

## Configuration
The configuration is done via environment variables. The following variables are available:

### Wikidata REST Component

| Variable                                                | Description                                                       | Default                                                                                            |
|---------------------------------------------------------|-------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| WIKIDATA_SERVICE_LOG_FILE                               | Path to the log file                                              |                                                                                                    |
| WIKIDATA_SERVICE_LOG_LEVEL                              | Log level (e.g., DEBUG, INFO, WARNING, ERROR)                     | DEBUG                                                                                              |
| WIKIDATA_SERVICE_LOG_FORMAT                             | Log format                                                        | %{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message} |
| WIKIDATA_SERVICE_CACHE_FOLDER                           | Folder path for caching data                                      | data/service                                                                                       |
| WIKIDATA_SERVICE_CACHE_TTL_DURATION_IN_DAYS             | Time-to-live for cache in days                                    | 30                                                                                                 |
| WIKIDATA_SERVICE_ID_HUB_CACHE_FOLDER                    | Folder path for caching ID hub data                               | data/hub                                                                                           |
| WIKIDATA_SERVICE_CLEAR_CACHE                            | Whether to clear cache on startup                                 | true                                                                                               |
| WIKIDATA_SERVICE_HTTP_ADDRESS                           | HTTP server address and port                                      | localhost:3000                                                                                     |
| WIKIDATA_SERVICE_EXTERNAL_HTTP_ADDRESS                  | External HTTP server address and port                             | http://localhost:3000                                                                              |
| WIKIDATA_SERVICE_CERTIFICATE                            | Path to TLS certificate file (if TLS is enabled)                  |                                                                                                    |
| WIKIDATA_SERVICE_CERTIFICATE_KEY                        | Path to TLS certificate key file (if TLS is enabled)              |                                                                                                    |
| WIKIDATA_SERVICE_METADATA_LANGUAGES                     | Languages for metadata retrieval (comma-separated list)           | en,de,fr,it,rm                                                                                     |
| WIKIDATA_SERVICE_WIKIDATA_PROPERTIES                    | Wikidata properties to retrieve (comma-separated list)            |                                                                                                    |
| WIKIDATA_SERVICE_WIKIDATA_ITEM_FIELDS                   | Fields to retrieve for Wikidata items (comma-separated list)      | type,labels,descriptions,statements,sitelinks                                                      |
| WIKIDATA_SERVICE_ELASTICSEARCH_CACHE_ENABLED            | Whether Elasticsearch caching is enabled                          | false                                                                                              |
| WIKIDATA_SERVICE_ELASTICSEARCH_NODES                    | Elasticsearch node URLs (comma-separated list)                    | http://localhost:8080                                                                              |
| WIKIDATA_SERVICE_ELASTICSEARCH_CACHE_INDEX              | Elasticsearch cache index name                                    | alma-wikidata-service-cache-test                                                                   |
| WIKIDATA_SERVICE_ELASTICSEARCH_CACHE_INDEX_MAPPING_PATH | Path to Elasticsearch cache index mapping file                    | mappings/cache.mapping.json                                                                        |
| WIKIDATA_SERVICE_ELASTICSEARCH_HUB_INDEX                | Elasticsearch hub index name                                      | alma-wikidata-service-hub-cache-test                                                               |
| WIKIDATA_SERVICE_ELASTICSEARCH_HUB_INDEX_MAPPING_PATH   | Path to Elasticsearch hub index mapping file                      | mappings/hub.mapping.json                                                                          |
| WIKIDATA_SERVICE_ELASTICSEARCH_API_KEY                  | API key for Elasticsearch (if applicable)                         |                                                                                                    |
| WIKIDATA_SERVICE_ELASTICSEARCH_CA_CERT                  | Path to CA certificate file for Elasticsearch (if TLS is enabled) | secrets/ca.cert.pem                                                                                |

### Wikidata MARC Service Component

| Variable                                                        | Description                                                              | Default                                                                                            |
|-----------------------------------------------------------------|--------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| WIKIDATA_MARC_SERVICE_LOG_FILE                                  | Path to the log file                                                     |                                                                                                    |
| WIKIDATA_MARC_SERVICE_LOG_LEVEL                                 | Log level (e.g., DEBUG, INFO, WARNING, ERROR)                            | DEBUG                                                                                              |
| WIKIDATA_MARC_SERVICE_LOG_FORMAT                                | Log format                                                               | %{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message} |
| WIKIDATA_MARC_SERVICE_HTTP_ADDRESS                              | HTTP server address and port                                             | localhost:5000                                                                                     |
| WIKIDATA_MARC_SERVICE_EXTERNAL_HTTP_ADDRESS                     | External HTTP server address and port                                    | http://localhost:5000                                                                              |
| WIKIDATA_MARC_SERVICE_CERTIFICATE                               | Path to TLS certificate file (enables TLS when set)                      |                                                                                                    |
| WIKIDATA_MARC_SERVICE_CERTIFICATE_KEY                           | Path to TLS certificate key file (enables TLS when set)                  |                                                                                                    |
| WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_URL                      | URL for Wikidata service API endpoint                                    | http://localhost:5000/api/v1                                                                       |
| WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_FILTER_LANGUAGES         | Languages to filter results from Wikidata service (comma-separated list) | de,fr,it,en,rm                                                                                     |
| WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_FILTER_PROPERTIES        | Wikidata properties to filter results (comma-separated list)             | P31,P18                                                                                            |
| WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_FILTER_INCLUDE_SITELINKS | Whether to include site links in filtered results                        | true                                                                                               |


## Install Dependencies
With these commands the dependencies can be installed. Most likely only one of them needs to be run to execute the project.

```shell
go get -u ./cmd/wikidata

go get -u ./cmd/marc21
```


## API Docs Generation
The documentation is generated with [swag](https://github.com/swaggo/swag). Use the following command to install swag:

```shell
go install github.com/swaggo/swag/cmd/swag@latest
```

To generate the swagger docs use the following commands:

```shell
cd pkg/service
/usr/local/go/bin/go generate -run "swag init --parseDependency  --parseInternal -g ./api.go"

cd ../../pkg/service_marc
/usr/local/go/bin/go generate -run "swag init --parseDependency  --parseInternal -g ./api.go"
```


## Development Requirements

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Go](https://go.dev/doc/install)
- [Docker Engine](https://docs.docker.com/engine/install/)
- Any IDE or text editor

### Build Docker Image
Use this command to build the image locally. For production environments it is expected to run this container from the 
image generated for the GitLab container registry.

It is necessary to install docker and have the user in the sudo docker group in order to build an image.

In order to integrate the latest changes in the docker image it is necessary to create a go release of the packages. To
do so a git tag has to be added and pushed to the remote. The docker build process always loads the latest git tag when installing
the go package.

```shell
docker build -t test-service .
```

### Run Docker Container
This command can be used to run the docker service locally. This can be used after the docker image has been built.
It requires the following files to run:

```shell
docker run -p 8080:8080 --rm --name wikidata-rest-service test-service
```

```shell
docker run -p 8081:8081 --rm --entrypoint "/app/marc21" --name wikidata-marc21-service test-service
```