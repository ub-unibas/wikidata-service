FROM golang:1.21.6 as builder

ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.switch.ch/ub-unibas/*
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64


WORKDIR /source
COPY . /source
RUN go mod download
WORKDIR /source/cmd/wikidata
RUN go build -o /app/wikidata
WORKDIR /source/cmd/marc21
RUN go build -o /app/marc21
WORKDIR /source/cmd/propertyexplorer
RUN go build -o /app/propertyexplorer

FROM alpine:latest
WORKDIR /
COPY --from=builder /app /app

ENTRYPOINT ["/app/wikidata"]