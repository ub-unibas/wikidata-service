package main

import (
	"github.com/je4/utils/v2/pkg/zLogger"
	"log"
	"os"
	"strconv"
	"strings"
)

type ConfigValue struct {
	EnvVar          string
	DefaultValue    string
	HasDefaultValue bool
}

func NewConfigValue(envVar string, defaultValue string, hasDefaultValue bool) string {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    defaultValue,
		HasDefaultValue: hasDefaultValue,
	}.Load()
}

func NewConfigValueBool(envVar string, defaultValue bool, hasDefaultValue bool) bool {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    "",
		HasDefaultValue: hasDefaultValue,
	}.LoadBool(defaultValue)
}

func NewConfigValueInt(envVar string, defaultValue int, hasDefaultValue bool) int {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    "",
		HasDefaultValue: hasDefaultValue,
	}.LoadInt(defaultValue)
}

func (cv ConfigValue) LoadBool(defaultValue bool) bool {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		return defaultValue
	}
	return strings.ToLower(val) == "true"
}

func (cv ConfigValue) LoadInt(defaultValue int) int {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		return defaultValue
	}
	num, err := strconv.Atoi(val)

	if err != nil {
		log.Fatalf("Could not parse integer from string for setting: %s", val)
	}
	return num
}

func NewConfigValueList(envVar string, defaultValue string, hasDefaultValue bool) []string {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    defaultValue,
		HasDefaultValue: hasDefaultValue,
	}.LoadList()
}

func (cv ConfigValue) Load() string {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		val = cv.DefaultValue
	}
	return val
}

func (cv ConfigValue) LoadList() []string {
	val := os.Getenv(cv.EnvVar)

	if val == "" && cv.HasDefaultValue {
		val = cv.DefaultValue
	}
	values := strings.Split(val, ",")
	var nonEmptyValues []string
	for _, v := range values {
		trimmedValue := strings.TrimSpace(v)
		if trimmedValue != "" {
			nonEmptyValues = append(nonEmptyValues, trimmedValue)
		}
	}
	return nonEmptyValues
}

type WikidataServiceConfig struct {
	LogFile   string
	LogLevel  string
	LogFormat string

	UseLogstash       bool
	LogstashHost      string
	LogstashPort      int
	LogstashNamespace string
	LogstashLogLevel  string

	CacheFolder         string
	CacheTTLDays        int
	IdHubCacheFolder    string
	ClearCache          bool
	HttpAddress         string
	ExternalHttpAddress string
	TLSCert             string
	TLSKey              string

	UseTLS             bool
	MetadataLanguages  []string
	WikidataProperties []string
	WikidataItemFields string

	ElasticsearchCacheEnabled          bool
	ElasticsearchNodes                 []string
	ElasticsearchCacheIndex            string
	ElasticsearchCacheIndexMappingPath string
	ElasticsearchHubIndex              string
	ElasticsearchHubIndexMappingPath   string
	ElasticsearchApiKey                string
	TLSCaCert                          string
}

func LoadConfig() (*WikidataServiceConfig, error) {
	defaultConf := &WikidataServiceConfig{
		LogFile:  NewConfigValue("WIKIDATA_SERVICE_LOG_FILE", "", true),
		LogLevel: NewConfigValue("WIKIDATA_SERVICE_LOG_LEVEL", "DEBUG", true),
		LogFormat: NewConfigValue("WIKIDATA_SERVICE_LOG_FORMAT",
			"%{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message}",
			true),
		UseLogstash:                        NewConfigValueBool("WIKIDATA_SERVICE_USE_LOGSTASH", true, true),
		LogstashHost:                       NewConfigValue("WIKIDATA_SERVICE_LOGSTASH_HOST", "localhost", true),
		LogstashPort:                       NewConfigValueInt("WIKIDATA_SERVICE_LOGSTASH_PORT", 5046, true),
		LogstashNamespace:                  NewConfigValue("WIKIDATA_SERVICE_LOGSTASH_NAMESPACE", "wikidata-service", true),
		LogstashLogLevel:                   NewConfigValue("WIKIDATA_SERVICE_LOGSTASH_LOG_LEVEL", "DEBUG", true),
		CacheFolder:                        NewConfigValue("WIKIDATA_SERVICE_CACHE_FOLDER", "data/service", true),
		CacheTTLDays:                       NewConfigValueInt("WIKIDATA_SERVICE_CACHE_TTL_DURATION_IN_DAYS", 30, true),
		IdHubCacheFolder:                   NewConfigValue("WIKIDATA_SERVICE_ID_HUB_CACHE_FOLDER", "data/hub", true),
		ClearCache:                         NewConfigValueBool("WIKIDATA_SERVICE_CLEAR_CACHE", true, true),
		HttpAddress:                        NewConfigValue("WIKIDATA_SERVICE_HTTP_ADDRESS", "localhost:3000", true),
		ExternalHttpAddress:                NewConfigValue("WIKIDATA_SERVICE_EXTERNAL_HTTP_ADDRESS", "http://localhost:3000", true),
		TLSCert:                            NewConfigValue("WIKIDATA_SERVICE_CERTIFICATE", "", false),
		TLSKey:                             NewConfigValue("WIKIDATA_SERVICE_CERTIFICATE_KEY", "", false),
		UseTLS:                             NewConfigValueBool("WIKIDATA_MARC_SERVICE_USE_TLS", false, true),
		MetadataLanguages:                  NewConfigValueList("WIKIDATA_SERVICE_METADATA_LANGUAGES", "en,de,fr,it,rm", true),
		WikidataProperties:                 NewConfigValueList("WIKIDATA_SERVICE_WIKIDATA_PROPERTIES", "", true),
		WikidataItemFields:                 NewConfigValue("WIKIDATA_SERVICE_WIKIDATA_ITEM_FIELDS", "type,labels,descriptions,statements,sitelinks", true),
		ElasticsearchCacheEnabled:          NewConfigValueBool("WIKIDATA_SERVICE_ELASTICSEARCH_CACHE_ENABLED", false, true),
		ElasticsearchNodes:                 NewConfigValueList("WIKIDATA_SERVICE_ELASTICSEARCH_NODES", "http://localhost:8080", true),
		ElasticsearchCacheIndex:            NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_CACHE_INDEX", "alma-wikidata-service-cache-test", true),
		ElasticsearchCacheIndexMappingPath: NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_CACHE_INDEX_MAPPING_PATH", "mappings/cache.mapping.json", true),
		ElasticsearchHubIndex:              NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_HUB_INDEX", "alma-wikidata-service-hub-cache-test", true),
		ElasticsearchHubIndexMappingPath:   NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_HUB_INDEX_MAPPING_PATH", "mappings/hub.mapping.json", true),
		ElasticsearchApiKey:                NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_API_KEY", "", false),
		TLSCaCert:                          NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_CA_CERT", "secrets/ca.cert.pem", true),
	}

	return defaultConf, nil
}

func (c WikidataServiceConfig) DisplayConfigLog(logger zLogger.ZLogger) {
	logger.Info().Msg("Wikidata Service Configuration:")
	logger.Info().Msgf("LogFile: %s", c.LogFile)
	logger.Info().Msgf("LogLevel: %s", c.LogLevel)
	logger.Info().Msgf("LogFormat: %s", c.LogFormat)
	logger.Info().Msgf("UseLogstash: %v", c.UseLogstash)
	logger.Info().Msgf("LogstashHost: %s", c.LogstashHost)
	logger.Info().Msgf("LogstashPort: %d", c.LogstashPort)
	logger.Info().Msgf("LogstashNamespace: %s", c.LogstashNamespace)
	logger.Info().Msgf("LogstashLogLevel: %s", c.LogstashLogLevel)
	logger.Info().Msgf("CacheFolder: %s", c.CacheFolder)
	logger.Info().Msgf("IdHubCacheFolder: %s", c.IdHubCacheFolder)
	logger.Info().Msgf("ClearCache: %v", c.ClearCache)
	logger.Info().Msgf("HttpAddress: %s", c.HttpAddress)
	logger.Info().Msgf("ExternalHttpAddress: %s", c.ExternalHttpAddress)
	logger.Info().Msgf("TLSCert: %s", c.TLSCert)
	logger.Info().Msgf("TLSKey: %s", c.TLSKey)
	logger.Info().Msgf("UseTLS: %v", c.UseTLS)
	logger.Info().Msgf("MetadataLanguages: %s", strings.Join(c.MetadataLanguages, ","))
	logger.Info().Msgf("WikidataProperties: %s", strings.Join(c.WikidataProperties, ","))
	logger.Info().Msgf("WikidataItemFields: %s", c.WikidataItemFields)
	logger.Info().Msgf("ElasticsearchCacheEnabled: %v", c.ElasticsearchCacheEnabled)
	logger.Info().Msgf("ElasticsearchNodes: %s", strings.Join(c.ElasticsearchNodes, ","))
	logger.Info().Msgf("ElasticsearchCacheIndex: %s", c.ElasticsearchCacheIndex)
	logger.Info().Msgf("ElasticsearchCacheIndexMappingPath: %s", c.ElasticsearchCacheIndexMappingPath)
	logger.Info().Msgf("ElasticsearchHubIndex: %s", c.ElasticsearchHubIndex)
	logger.Info().Msgf("ElasticsearchHubIndexMappingPath: %s", c.ElasticsearchHubIndexMappingPath)
	logger.Info().Msgf("TLSCaCert: %s", c.TLSCaCert)
}
