package main

import (
	"crypto/tls"
	"github.com/je4/utils/v2/pkg/zLogger"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/service"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"log"
	"os"
	"os/signal"
	"syscall"

	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
)

func main() {
	config, err := LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load configuration file: %v", err)
		return
	}
	var logger zLogger.ZLogger
	if config.UseLogstash {
		logstashTlsConfig, logstashErr := shared.LoadLogstashCert(config.TLSCaCert, config.TLSCert, config.TLSKey)
		if logstashErr != nil {
			log.Fatalf("Cannot load logstash tls certificate: %v", logstashErr)
			return
		}
		logstashLogger, logstash, _ := ublogger.CreateUbMultiLoggerTLS(config.LogLevel, config.LogFile,
			ublogger.SetLogStash(config.LogstashHost, config.LogstashPort, config.LogstashNamespace, config.LogstashLogLevel),
			ublogger.SetTLS(true),
			ublogger.SetTLSConfig(logstashTlsConfig),
			ublogger.SetSkipVerify(false),
		)
		defer logstash.Close()
		logger = logstashLogger
	} else {
		consoleLogger, _, _ := ublogger.CreateUbMultiLogger("", 0, config.LogLevel, "", config.LogFile, "wikidata-service")
		logger = consoleLogger
	}

	logger.Info().Msg("Starting Wikidata Service")
	config.DisplayConfigLog(logger)
	var tlsConfig *tls.Config
	if config.UseTLS {
		logger.Info().Msg("Using TLS for wikidata service.")
		var tlsErr error
		tlsConfig, tlsErr = shared.LoadCertificateConfig(
			config.TLSCert, config.TLSKey)
		if tlsErr != nil {
			log.Fatalf("Cannot load tls certificate: %v", tlsErr)
			return
		}
	} else {
		logger.Info().Msg("Not using TLS for wikidata service.")
		tlsConfig = nil
	}
	httpClient := shared.NewServiceHttpClient(tlsConfig)
	var database *service.WikidataCache
	var elasticsearch *shared.Elastic
	var hubController *shared.IdHubController
	if config.ElasticsearchCacheEnabled {
		caCert := shared.LoadCaCertificate(config.TLSCaCert, logger)
		elasticsearch, err = shared.EstablishNewClient(
			config.ElasticsearchNodes,
			config.ElasticsearchCacheIndex,
			config.ElasticsearchApiKey,
			config.ElasticsearchCacheIndexMappingPath,
			caCert,
			config.ClearCache,
			logger,
		)
		if err != nil {
			logger.Fatal().Err(err).Msg("Cannot establish Elasticsearch client")
			return
		}
		idHubElastic, idHubElasticErr := shared.EstablishNewClient(
			config.ElasticsearchNodes,
			config.ElasticsearchHubIndex,
			config.ElasticsearchApiKey,
			config.ElasticsearchHubIndexMappingPath,
			caCert,
			config.ClearCache,
			logger,
		)
		if idHubElasticErr != nil {
			logger.Fatal().Err(err).Msg("Cannot establish elasticsearch client for id hub.")
		}
		hubController = shared.EstablishIdHubControllerWithElasticCache(idHubElastic, &logger)
	} else {
		database, err = service.EstablishCache(config.CacheFolder, config.CacheTTLDays)
		if err != nil {
			logger.Fatal().Err(err).Msg("Cannot establish cache for Wikidata Service with database folder " + config.CacheFolder)
			return
		}
		if config.ClearCache {
			err = database.Clear()
			if err != nil {
				logger.Fatal().Err(err).Msg("Cannot clear cache")
				return
			}
		}

		defer database.Close()
		hubController, err = shared.EstablishIdHubController(config.ClearCache, config.IdHubCacheFolder, &logger)
		if err != nil {
			logger.Fatal().Err(err).Msgf("Cannot establish id hub controller: %v", err)
			return
		}
	}

	controller, err := service.EstablishWikidataServiceController(config.HttpAddress,
		config.ExternalHttpAddress,
		config.MetadataLanguages,
		config.WikidataProperties,
		config.WikidataItemFields,
		tlsConfig,
		httpClient,
		database,
		config.ElasticsearchCacheEnabled,
		elasticsearch,
		hubController,
		&logger)
	defer controller.GracefulStop()

	logger.Info().Msg("Wikidata Service started.")
	controller.Start()
	logger.Info().Msgf("Open docs here: %s/swagger/index.html or %s/swagger/index.html", config.HttpAddress, config.ExternalHttpAddress)
	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	logger.Info().Msg("Press ctrl+c to stop server.")
	s := <-done
	logger.Info().Msg("Got signal: " + s.String())
}
