package main

import (
	"github.com/je4/utils/v2/pkg/zLogger"
	"log"
	"os"
	"strconv"
	"strings"
)

type ConfigValue struct {
	EnvVar          string
	DefaultValue    string
	HasDefaultValue bool
}

func NewConfigValue(envVar string, defaultValue string, hasDefaultValue bool) string {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    defaultValue,
		HasDefaultValue: hasDefaultValue,
	}.Load()
}

func NewConfigValueBool(envVar string, defaultValue bool, hasDefaultValue bool) bool {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    "",
		HasDefaultValue: hasDefaultValue,
	}.LoadBool(defaultValue)
}

func NewConfigValueInt(envVar string, defaultValue int, hasDefaultValue bool) int {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    "",
		HasDefaultValue: hasDefaultValue,
	}.LoadInt(defaultValue)
}

func (cv ConfigValue) LoadBool(defaultValue bool) bool {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		return defaultValue
	}
	return strings.ToLower(val) == "true"
}

func (cv ConfigValue) LoadInt(defaultValue int) int {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		return defaultValue
	}
	num, err := strconv.Atoi(val)

	if err != nil {
		log.Fatalf("Could not parse integer from string for setting: %s", val)
	}
	return num
}

func NewConfigValueList(envVar string, defaultValue string, hasDefaultValue bool) []string {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    defaultValue,
		HasDefaultValue: hasDefaultValue,
	}.LoadList()
}

func (cv ConfigValue) Load() string {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		val = cv.DefaultValue
	}
	return val
}

func (cv ConfigValue) LoadList() []string {
	val := os.Getenv(cv.EnvVar)

	if val == "" && cv.HasDefaultValue {
		val = cv.DefaultValue
	}
	values := strings.Split(val, ",")
	var nonEmptyValues []string
	for _, v := range values {
		trimmedValue := strings.TrimSpace(v)
		if trimmedValue != "" {
			nonEmptyValues = append(nonEmptyValues, trimmedValue)
		}
	}
	return nonEmptyValues
}

type WikidataServiceConfig struct {
	LogFile   string
	LogLevel  string
	LogFormat string

	OutputFilePath string

	ElasticsearchNodes      []string
	ElasticsearchCacheIndex string
	ElasticsearchHubIndex   string
	ElasticsearchApiKey     string
	TLSCaCert               string
}

func LoadConfig() (*WikidataServiceConfig, error) {
	defaultConf := &WikidataServiceConfig{
		LogFile:  NewConfigValue("WIKIDATA_SERVICE_LOG_FILE", "", true),
		LogLevel: NewConfigValue("WIKIDATA_SERVICE_LOG_LEVEL", "DEBUG", true),
		LogFormat: NewConfigValue("WIKIDATA_SERVICE_LOG_FORMAT",
			"%{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message}",
			true),
		OutputFilePath:          NewConfigValue("WIKIDATA_SERVICE_OUTPUT_FILE_PATH", "output.csv", true),
		ElasticsearchNodes:      NewConfigValueList("WIKIDATA_SERVICE_ELASTICSEARCH_NODES", "https://localhost:8080", true),
		ElasticsearchCacheIndex: NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_CACHE_INDEX", "alma-wikidata-service-cache-test-v5", true),
		ElasticsearchHubIndex:   NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_HUB_INDEX", "alma-wikidata-service-hub-cache-test", true),
		ElasticsearchApiKey:     NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_API_KEY", "", false),
		TLSCaCert:               NewConfigValue("WIKIDATA_SERVICE_ELASTICSEARCH_CA_CERT", "secrets/ca.cert.pem", true),
	}

	return defaultConf, nil
}

func (c WikidataServiceConfig) DisplayConfigLog(logger zLogger.ZLogger) {
	logger.Info().Msg("Property Explorer Configuration:")
	logger.Info().Msgf("  LogFile: %s", c.LogFile)
	logger.Info().Msgf("  LogLevel: %s", c.LogLevel)
	logger.Info().Msgf("  LogFormat: %s", c.LogFormat)
	logger.Info().Msgf("  ElasticsearchNodes: %s", strings.Join(c.ElasticsearchNodes, ","))
	logger.Info().Msgf("  ElasticsearchCacheIndex: %s", c.ElasticsearchCacheIndex)
	logger.Info().Msgf("  ElasticsearchHubIndex: %s", c.ElasticsearchHubIndex)
	logger.Info().Msgf("  TLSCaCert: %s", c.TLSCaCert)
}
