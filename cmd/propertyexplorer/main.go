package main

import (
	"encoding/csv"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"log"
	"os"
	"strconv"
)

func main() {
	config, err := LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load configuration file: %v", err)
		return
	}

	var logger, errLogger = shared.DefineLogger(config.LogFile, config.LogLevel)
	if errLogger != nil {
		log.Fatalf("Cannot define logger: %v", err)
		return
	}
	config.DisplayConfigLog(&logger)

	caCert := shared.LoadCaCertificate(config.TLSCaCert, &logger)
	client, err := shared.EstablishNewClientNoNewIndex(
		config.ElasticsearchNodes,
		config.ElasticsearchCacheIndex,
		config.ElasticsearchApiKey,
		caCert,
		&logger,
	)
	if err != nil {
		logger.Fatal().Err(err).Msg("cannot establish client")
		return
	}

	keyCounts, err := client.ScrollIndex()
	if err != nil {
		logger.Fatal().Err(err).Msg("search failed")
		return
	}
	logger.Info().Msg("Finished scrolling index.")
	logger.Info().Msg("Start writing to file.")
	file, err := os.Create(config.OutputFilePath)
	if err != nil {
		log.Fatalln("Failed to open file", err)
	}
	defer func(file *os.File) {
		err3 := file.Close()
		if err3 != nil {
			log.Fatalln("Failed to close file", err3)
		}
	}(file)
	writer := csv.NewWriter(file)
	defer writer.Flush()

	err = writer.Write([]string{"Key", "Count"})
	if err != nil {
		log.Fatal("Could not write to file", err)
	}

	wikidataRest := shared.NewWikidataRestClient(
		make([]string, 0),
		make([]string, 0),
		"",
		shared.NewServiceHttpClient(nil),
		&logger,
	)

	for key, count := range keyCounts {
		propertyName, err := wikidataRest.GetPropertyName(key)
		if err != nil {
			log.Fatalf("Could not get property name for key %s", key)
		}
		err = writer.Write([]string{key, strconv.Itoa(count), propertyName})
		if err != nil {
			log.Fatal("Could not write to file", err)
		}
	}
	logger.Info().Msg("Finished writing to file.")
}
