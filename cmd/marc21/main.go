package main

import (
	"crypto/tls"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/service_marc"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	config, err := LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load configuration file: %v", err)
		return
	}

	var logger *ublogger.Logger
	if config.UseLogstash {
		logstashTlsConfig, logstashErr := shared.LoadLogstashCert(config.TLSCaCert, config.TLSCert, config.TLSKey)
		if logstashErr != nil {
			log.Fatalf("Cannot load logstash tls certificate: %v", logstashErr)
			return
		}
		logstashLogger, logstash, _ := ublogger.CreateUbMultiLoggerTLS(config.LogLevel, config.LogFile,
			ublogger.SetLogStash(config.LogstashHost, config.LogstashPort, config.LogstashNamespace, config.LogstashLogLevel),
			ublogger.SetTLS(true),
			ublogger.SetTLSConfig(logstashTlsConfig),
			ublogger.SetSkipVerify(false),
		)
		defer logstash.Close()
		logger = logstashLogger
	} else {
		consoleLogger, _, _ := ublogger.CreateUbMultiLogger("", 0, config.LogLevel, "", config.LogFile, "wikidata-service")
		logger = consoleLogger
	}
	logger.Info().Msg("Starting Wikidata MARC Service")
	config.DisplayConfigLog(logger)

	var tlsConfig *tls.Config
	if config.UseTLS {
		logger.Info().Msg("Using TLS for wikidata service.")
		var tlsErr error
		tlsConfig, tlsErr = shared.LoadCertificateConfig(
			config.TLSCert, config.TLSKey)
		if tlsErr != nil {
			log.Fatalf("Cannot load tls certificate: %v", tlsErr)
			return
		}
	} else {
		logger.Info().Msg("Not using TLS for wikidata service.")
		tlsConfig = nil
	}
	if err != nil {
		return
	}
	controller, err := service_marc.NewController(
		config.HttpAddress,
		config.ExternalHttpAddress,
		tlsConfig,
		config.WikidataServiceUrl,
		config.WikidataServiceFilterLanguages,
		config.WikidataServiceFilerProperties,
		config.WikidataServiceFilterSiteLinks,
		tlsConfig,
		logger)
	if err != nil {
		logger.Fatal().Msgf("Failed to initialize controller: %s", err)
	}
	controller.Start()
	logger.Info().Msgf("Open docs here: %s/swagger/index.html or %s/swagger/index.html", config.HttpAddress, config.ExternalHttpAddress)

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	logger.Info().Msg("Service is running. Terminate the server with CTRL+C.")
	s := <-done
	logger.Info().Msgf("Got signal %s. Shutting down service.", s)
	controller.GracefulStop()
}
