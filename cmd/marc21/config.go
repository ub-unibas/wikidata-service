package main

import (
	"github.com/je4/utils/v2/pkg/zLogger"
	"log"
	"os"
	"strconv"
	"strings"
)

type ConfigValue struct {
	EnvVar          string
	DefaultValue    string
	HasDefaultValue bool
}

func NewConfigValue(envVar string, defaultValue string, hasDefaultValue bool) string {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    defaultValue,
		HasDefaultValue: hasDefaultValue,
	}.Load()
}

func NewConfigValueInt(envVar string, defaultValue int, hasDefaultValue bool) int {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    "",
		HasDefaultValue: hasDefaultValue,
	}.LoadInt(defaultValue)
}

func (cv ConfigValue) LoadInt(defaultValue int) int {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		return defaultValue
	}
	num, err := strconv.Atoi(val)

	if err != nil {
		log.Fatalf("Could not parse integer from string for setting: %s", val)
	}
	return num
}

func NewConfigValueBool(envVar string, defaultValue bool, hasDefaultValue bool) bool {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    "",
		HasDefaultValue: hasDefaultValue,
	}.LoadBool(defaultValue)
}

func (cv ConfigValue) LoadBool(defaultValue bool) bool {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		return defaultValue
	}
	return strings.ToLower(val) == "true"
}

func NewConfigValueList(envVar string, defaultValue string, hasDefaultValue bool) []string {
	return ConfigValue{
		EnvVar:          envVar,
		DefaultValue:    defaultValue,
		HasDefaultValue: hasDefaultValue,
	}.LoadList()
}

func (cv ConfigValue) Load() string {
	val := os.Getenv(cv.EnvVar)
	if val == "" && cv.HasDefaultValue {
		val = cv.DefaultValue
	}
	return val
}

func (cv ConfigValue) LoadList() []string {
	val := os.Getenv(cv.EnvVar)

	if val == "" && cv.HasDefaultValue {
		val = cv.DefaultValue
	}
	values := strings.Split(val, ",")
	var nonEmptyValues []string
	for _, v := range values {
		trimmedValue := strings.TrimSpace(v)
		if trimmedValue != "" {
			nonEmptyValues = append(nonEmptyValues, trimmedValue)
		}
	}
	return nonEmptyValues
}

type WikidataServiceConfig struct {
	LogFile   string
	LogLevel  string
	LogFormat string

	UseLogstash         bool
	LogstashHost        string
	LogstashPort        int
	LogstashNamespace   string
	LogstashLogLevel    string
	HttpAddress         string
	ExternalHttpAddress string
	TLSCert             string
	TLSKey              string

	UseTLS                         bool
	WikidataServiceUrl             string
	WikidataServiceFilterLanguages []string
	WikidataServiceFilerProperties []string
	WikidataServiceFilterSiteLinks bool
	TLSCaCert                      string
}

func LoadConfig() (*WikidataServiceConfig, error) {
	defaultConf := &WikidataServiceConfig{
		LogFile:  NewConfigValue("WIKIDATA_MARC_SERVICE_LOG_FILE", "", true),
		LogLevel: NewConfigValue("WIKIDATA_MARC_SERVICE_LOG_LEVEL", "DEBUG", true),
		LogFormat: NewConfigValue("WIKIDATA_MARC_SERVICE_LOG_FORMAT",
			"%{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message}",
			true),
		UseLogstash:                    NewConfigValueBool("WIKIDATA_SERVICE_USE_LOGSTASH", false, true),
		LogstashHost:                   NewConfigValue("WIKIDATA_SERVICE_LOGSTASH_HOST", "localhost", true),
		LogstashPort:                   NewConfigValueInt("WIKIDATA_SERVICE_LOGSTASH_PORT", 5049, true),
		LogstashNamespace:              NewConfigValue("WIKIDATA_SERVICE_LOGSTASH_NAMESPACE", "wikidata-service", true),
		LogstashLogLevel:               NewConfigValue("WIKIDATA_SERVICE_LOGSTASH_LOG_LEVEL", "DEBUG", true),
		HttpAddress:                    NewConfigValue("WIKIDATA_MARC_SERVICE_HTTP_ADDRESS", "localhost:5000", true),
		ExternalHttpAddress:            NewConfigValue("WIKIDATA_MARC_SERVICE_EXTERNAL_HTTP_ADDRESS", "http://localhost:5000", true),
		TLSCert:                        NewConfigValue("WIKIDATA_MARC_SERVICE_CERTIFICATE", "", false),
		TLSKey:                         NewConfigValue("WIKIDATA_MARC_SERVICE_CERTIFICATE_KEY", "", false),
		UseTLS:                         NewConfigValueBool("WIKIDATA_MARC_SERVICE_USE_TLS", false, true),
		WikidataServiceUrl:             NewConfigValue("WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_URL", "http://localhost:5000/api/v1", true),
		WikidataServiceFilterLanguages: NewConfigValueList("WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_FILTER_LANGUAGES", "de,fr,it,en,rm", true),
		WikidataServiceFilerProperties: NewConfigValueList("WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_FILTER_PROPERTIES", "P31,P18", true),
		WikidataServiceFilterSiteLinks: NewConfigValueBool("WIKIDATA_MARC_SERVICE_WIKIDATA_SERVICE_FILTER_INCLUDE_SITELINKS", true, true),
		TLSCaCert:                      NewConfigValue("WIKIDATA_MARC_SERVICE_CA_CERT", "", false),
	}

	return defaultConf, nil
}

func (c WikidataServiceConfig) DisplayConfigLog(logger zLogger.ZLogger) {
	logger.Info().Msg("Wikidata MARC21 Service Configuration:")
	logger.Info().Msgf("LogFile: %s", c.LogFile)
	logger.Info().Msgf("LogLevel: %s", c.LogLevel)
	logger.Info().Msgf("LogFormat: %s", c.LogFormat)
	logger.Info().Msgf("UseLogstash: %t", c.UseLogstash)
	logger.Info().Msgf("LogstashHost: %s", c.LogstashHost)
	logger.Info().Msgf("LogstashPort: %d", c.LogstashPort)
	logger.Info().Msgf("LogstashNamespace: %s", c.LogstashNamespace)
	logger.Info().Msgf("LogstashLogLevel: %s", c.LogstashLogLevel)
	logger.Info().Msgf("HttpAddress: %s", c.HttpAddress)
	logger.Info().Msgf("ExternalHttpAddress: %s", c.ExternalHttpAddress)
	logger.Info().Msgf("TLSCert: %s", c.TLSCert)
	logger.Info().Msgf("TLSKey: %s", c.TLSKey)
	logger.Info().Msgf("WikidataServiceURL: %s", c.WikidataServiceUrl)
	logger.Info().Msgf("WikidataServiceFilterLanguages: %s", c.WikidataServiceFilterLanguages)
	logger.Info().Msgf("WikidataServiceFilerProperties: %s", c.WikidataServiceFilerProperties)
	logger.Info().Msgf("WikidataServiceFilterSiteLinks: %t", c.WikidataServiceFilterSiteLinks)
	logger.Info().Msgf("TLSCaCert: %s", c.TLSCaCert)
}
