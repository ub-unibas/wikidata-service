package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/je4/utils/v2/pkg/zLogger"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"log"
	"os"
	"sort"
	"time"
)

func main() {
	urlFlag := flag.String("url", "", "URL to test")
	fileFlag := flag.String("file", "", "File containing URLs to test (one per line)")
	repeatFlag := flag.Int("repeat", 1, "Number of times to repeat each request")
	outputFlag := flag.String("output", "tests/results.csv", "Output CSV file for measurements")
	statisticsFlag := flag.String("statistics", "tests/statistics.csv", "Output CSV file for statistics")
	logFileFlag := flag.String("log", "tests/out.log", "The log file for the application.")
	flag.Parse()

	logger, err := shared.DefineLogger(*logFileFlag, "INFO")
	if err != nil {
		log.Fatalf("Could not initialize the logger with log file path %s.", *logFileFlag)
	}

	if (*urlFlag == "" && *fileFlag == "") || (*urlFlag != "" && *fileFlag != "") {
		logger.Fatal().Msgf("Please provide either a single URL using -url or a file of URLs using -file")
		os.Exit(1)
	}

	var urls []string
	if *urlFlag != "" {
		urls = append(urls, *urlFlag)
	} else {
		urls = readURLsFromFile(*fileFlag, &logger)
	}

	file, err := os.Create(*outputFlag)
	if err != nil {
		logger.Fatal().Msgf("Error creating CSV file: %v", err)
		os.Exit(1)
	}
	defer func(file *os.File) {
		errFile := file.Close()
		if errFile != nil {
			logger.Fatal().Msgf("Could not close file %v properly.", file)
			return
		}
	}(file)

	writer := csv.NewWriter(file)
	defer writer.Flush()

	writeError := writer.Write([]string{"URL", "RequestNumber", "DurationInSeconds"})
	if writeError != nil {
		logger.Fatal().Msgf("Error while writing CSV header: %v", writeError)
		return
	}
	var durations []float64

	client := resty.New()

	for _, url := range urls {
		for i := 1; i <= *repeatFlag; i++ {
			startTime := time.Now()

			response, responseError := client.R().Get(url)
			if responseError != nil {
				logger.Err(responseError).Msgf("Error making request to %s: %v\n", url, responseError)
				continue
			}

			duration := time.Since(startTime).Seconds()
			durations = append(durations, duration)

			writeError2 := writer.Write([]string{url, fmt.Sprint(i), fmt.Sprintf("%.6f", duration)})
			if writeError2 != nil {
				logger.Err(writeError2).Msgf("Could not write line to csv: %v.", writeError2)
				return
			}
			writer.Flush()

			logger.Info().Msgf("Request #%d to %s took %.6f seconds. Status: %s\n", i, url, duration, response.Status())
		}
	}
	storeStatistics(*statisticsFlag, durations, &logger)

	logger.Info().Msg("Finished testing performance.")
}

func storeStatistics(statisticsFile string, durations []float64, logger zLogger.ZLogger) {
	sort.Float64s(durations)

	minimum := durations[0]
	maximum := durations[len(durations)-1]

	var total float64
	for _, duration := range durations {
		total += duration
	}

	average := total / float64(len(durations))

	var median float64
	if len(durations)%2 == 0 {
		median = (durations[len(durations)/2-1] + durations[len(durations)/2]) / 2
	} else {
		median = durations[len(durations)/2]
	}
	writeStatistics(statisticsFile, minimum, maximum, average, median, total, logger)
}

func writeStatistics(statisticFile string, minimum, maximum, average, median, total float64, logger zLogger.ZLogger) {
	fileInfo, err := os.Stat(statisticFile)
	fileExists := !os.IsNotExist(err)

	var file *os.File

	if !fileExists {
		file, err = os.Create(statisticFile)
		if err != nil {
			logger.Fatal().Msgf("Error creating CSV file for statistics: %v", err)
			os.Exit(1)
		}
	} else {
		file, err = os.Open(statisticFile)
		if err != nil {
			logger.Fatal().Msgf("Error opening CSV file for statistics: %v", err)
			os.Exit(1)
		}
	}

	defer func(statistics *os.File) {
		errFile := statistics.Close()
		if errFile != nil {
			logger.Fatal().Msgf("Could not close statistics file properly.")
			return
		}
	}(file)

	statisticsWriter := csv.NewWriter(file)
	defer statisticsWriter.Flush()
	if !fileExists || fileInfo.Size() == 0 {
		header := []string{"min", "max", "avg", "median", "total"}
		writeError := statisticsWriter.Write(header)
		if writeError != nil {
			logger.Fatal().Msgf("Error while writing CSV header for statistics: %v", writeError)
			return
		}
		statisticsWriter.Flush()
	}

	writeError := statisticsWriter.Write([]string{fmt.Sprintf("%.6f", minimum),
		fmt.Sprintf("%.6f", maximum),
		fmt.Sprintf("%.6f", average),
		fmt.Sprintf("%.6f", median),
		fmt.Sprintf("%.6f", total),
	})
	if writeError != nil {
		logger.Fatal().Msgf("Could not write statistics to CSV: %v.", writeError)
		return
	}
	statisticsWriter.Flush()
}

func readURLsFromFile(file string, logger zLogger.ZLogger) []string {
	var urls []string

	fileHandle, err := os.Open(file)
	if err != nil {
		logger.Fatal().Msgf("Error reading file: %v", err)
		os.Exit(1)
	}
	defer func(fileHandle *os.File) {
		fileHandleError := fileHandle.Close()
		if fileHandleError != nil {
			logger.Fatal().Msgf("Could not close file properly: %v", fileHandleError)
			return
		}
	}(fileHandle)

	scanner := bufio.NewScanner(fileHandle)
	for scanner.Scan() {
		urls = append(urls, scanner.Text())
	}

	if scanUrlError := scanner.Err(); err != nil {
		logger.Fatal().Msgf("Error reading file: %v", scanUrlError)
		os.Exit(1)
	}

	return urls
}
