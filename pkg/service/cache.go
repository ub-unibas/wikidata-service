package service

import (
	"emperror.dev/errors"
	"encoding/json"
	"github.com/dgraph-io/badger/v4"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"time"
)

type WikidataCache struct {
	db   *badger.DB
	days int
}

func EstablishCache(folder string, days int) (*WikidataCache, error) {
	cache := &WikidataCache{db: nil, days: days}
	var err error
	if folder == "" {
		opt := badger.DefaultOptions("").WithInMemory(true)
		cache.db, err = badger.Open(opt)
	} else {
		cache.db, err = badger.Open(badger.DefaultOptions(folder))
		if err != nil {
			return nil, errors.Wrapf(err, "Cannot open badger in '%s'", folder)
		}
	}
	return cache, nil
}

func (cache *WikidataCache) Get(key string) (*shared.Resource, error) {
	var extractedValue []byte
	err := cache.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(key))
		if err != nil {
			if IsNotFound(err) {
				return shared.ResourceNotFoundError{Identifier: key}
			} else {
				return err
			}
		}
		// when the data inside is a pointer value then the extracted value stays nil.
		// so then it comes out of the data. This should always be the case here. Not sure why this is like this.
		data, err := item.ValueCopy(extractedValue)
		if err != nil {
			return err
		}
		extractedValue = data
		return nil
	})
	if err != nil {
		return nil, err
	}
	resource := &shared.Resource{}
	unmarshalError := json.Unmarshal(extractedValue, resource)
	if unmarshalError != nil {
		return nil, unmarshalError
	}
	return resource, err
}

func (cache *WikidataCache) Set(key string, value []byte) error {
	err := cache.db.Update(func(txn *badger.Txn) error {
		ttlDuration := time.Duration(cache.days) * 24 * time.Hour
		newEntry := badger.NewEntry([]byte(key), value).WithTTL(ttlDuration)
		err := txn.SetEntry(newEntry)
		return err
	})
	return err
}

func (cache *WikidataCache) Clear() error {
	err := cache.db.DropAll()
	return err
}

func (cache *WikidataCache) Close() {
	err := cache.db.Close()
	if err != nil {
		return
	}
}

func IsNotFound(err error) bool {
	return errors.Is(err, badger.ErrKeyNotFound)
}
