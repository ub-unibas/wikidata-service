definitions:
  shared.Data:
    properties:
      globeCoordinate:
        $ref: '#/definitions/shared.GlobeCoordinate'
      monoLingualText:
        $ref: '#/definitions/shared.MonoLingualText'
      quantity:
        $ref: '#/definitions/shared.Quantity'
      simple:
        type: string
      time:
        type: string
    type: object
  shared.GlobeCoordinate:
    properties:
      globe:
        type: string
      latitude:
        type: string
      longitude:
        type: string
      precision:
        type: number
    type: object
  shared.HTTPResultMessage:
    properties:
      code:
        example: 400
        type: integer
      message:
        example: status bad request
        type: string
    type: object
  shared.MonoLingualText:
    properties:
      language:
        type: string
      value:
        type: string
    type: object
  shared.Quantity:
    properties:
      amount:
        type: string
      lowerbound:
        type: string
      unit:
        type: string
      upperbound:
        type: string
    type: object
  shared.Resource:
    properties:
      data:
        additionalProperties:
          items:
            $ref: '#/definitions/shared.Data'
          type: array
        type: object
      descriptions:
        additionalProperties:
          type: string
        type: object
      error:
        type: string
      id:
        type: string
      labels:
        additionalProperties:
          type: string
        type: object
      lastModified:
        type: string
      originId:
        type: string
      siteLinks:
        additionalProperties:
          $ref: '#/definitions/shared.SiteLink'
        type: object
    type: object
  shared.SiteLink:
    properties:
      badges:
        items:
          type: string
        type: array
      title:
        type: string
      url:
        type: string
    type: object
  shared.WikidataResourceFilter:
    properties:
      includeSiteLinks:
        type: boolean
      languages:
        items:
          type: string
        type: array
      properties:
        items:
          type: string
        type: array
    type: object
info:
  contact:
    email: it-ub@unibas.ch
    name: University Library Basel, Informatik
    url: https://ub.unibas.ch
  description: Get Wikidata metadata from id
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  termsOfService: http://swagger.io/terms/
  title: Wikidata Service API
  version: "1.0"
paths:
  /{authority}/{identifier}:
    get:
      description: Gets the JSON representation of the service resource with the configured
        properties.
      parameters:
      - description: Authority type
        in: path
        name: authority
        required: true
        type: string
      - description: Authority identifier
        in: path
        name: identifier
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/shared.Resource'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/shared.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/shared.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/shared.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: Get service resource metadata
      tags:
      - Wikidata
    post:
      description: Get json representation of a wikidata resources with just the fields
        listed.
      parameters:
      - description: Authority type
        in: path
        name: authority
        required: true
        type: string
      - description: Authority identifier
        in: path
        name: identifier
        required: true
        type: string
      - description: Filter for Wikidata resource
        in: body
        name: filter
        required: true
        schema:
          $ref: '#/definitions/shared.WikidataResourceFilter'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/shared.Resource'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/shared.HTTPResultMessage'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/shared.HTTPResultMessage'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/shared.HTTPResultMessage'
      security:
      - ApiKeyAuth: []
      summary: Get wikidata resource filtered for fields.
      tags:
      - Wikidata
securityDefinitions:
  ApiKeyAuth:
    description: Bearer Authentication with JWT
    in: header
    name: Authorization
    type: apiKey
swagger: "2.0"
