//go:generate swag init --parseDependency  --parseInternal -g ./api.go

package service

import (
	"context"
	"crypto/tls"
	"emperror.dev/errors"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/je4/utils/v2/pkg/zLogger"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/service/docs"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"golang.org/x/net/http2"
	"net/http"
	"net/url"
	"strings"
)

const BasePath = "/api/v1"
const AuthorityParamWikidata = "wikidata"
const AuthorityParamGnd = "gnd"
const NoWikidataConnectionError = "NoWikidataConnection"

//	@title			Wikidata Service API
//	@version		1.0
//	@description	Get Wikidata metadata from id
//	@termsOfService	http://swagger.io/terms/

//	@contact.name	University Library Basel, Informatik
//	@contact.url	https://ub.unibas.ch
//	@contact.email	it-ub@unibas.ch

//	@license.name	Apache 2.0
//	@license.url	http://www.apache.org/licenses/LICENSE-2.0.html

// @securityDefinitions.apikey	ApiKeyAuth
// @in							header
// @name						Authorization
// @description					Bearer Authentication with JWT

func EstablishWikidataServiceController(addr, extAddr string,
	metadataLanguages, properties []string, fields string,
	tlsConfig *tls.Config,
	httpClient *http.Client,
	cache *WikidataCache,
	useElastic bool,
	elastic *shared.Elastic,
	hub *shared.IdHubController,
	logger *zLogger.ZLogger) (*Controller, error) {
	u, err := url.Parse(extAddr)
	if err != nil {
		return nil, errors.Wrapf(err, "invalid external address '%s'", extAddr)
	}
	subPath := "/" + strings.Trim(u.Path, "/")
	if len(u.Port()) == 0 {
		docs.SwaggerInfo.Host = u.Hostname()
	} else {
		docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", u.Hostname(), u.Port())
	}
	docs.SwaggerInfo.BasePath = "/" + strings.Trim(subPath+BasePath, "/")
	if tlsConfig == nil && len(u.Port()) != 0 {
		docs.SwaggerInfo.Schemes = []string{"http"}
	} else {
		docs.SwaggerInfo.Schemes = []string{"https"}
	}

	router := gin.Default()
	c := &Controller{
		addr:    addr,
		router:  router,
		subPath: subPath,
		cache:   cache,
		rest: shared.NewWikidataRestClient(
			metadataLanguages,
			properties,
			fields,
			httpClient,
			*logger,
		),
		useElastic: useElastic,
		elastic:    elastic,
		hub:        hub,
		logger:     *logger,
	}
	if initialErr := c.init(tlsConfig); initialErr != nil {
		return nil, errors.Wrap(initialErr, "cannot initialize rest controller")
	}
	return c, nil
}

type Controller struct {
	server     http.Server
	router     *gin.Engine
	addr       string
	subPath    string
	cache      *WikidataCache
	rest       *shared.WikidataRest
	useElastic bool
	elastic    *shared.Elastic
	hub        *shared.IdHubController
	logger     zLogger.ZLogger
}

func (ctrl *Controller) init(tlsConfig *tls.Config) error {
	var auth = func() gin.HandlerFunc {
		return func(c *gin.Context) {
			if len(c.GetHeader("Authorization")) == 0 {
				shared.NewResultMessage(c, http.StatusUnauthorized, errors.New("Authorization is required Header"))
				c.Abort()
			}
			c.Next()
		}
	}

	v1 := ctrl.router.Group(BasePath)
	//	v1.Use(auth())
	_ = auth
	v1.GET("/:authority/:identifier", ctrl.getWikidataResource)
	v1.POST("/:authority/:identifier", ctrl.getWikidataResourceFiltered)

	ctrl.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	ctrl.server = http.Server{
		Addr:      ctrl.addr,
		Handler:   ctrl.router,
		TLSConfig: tlsConfig,
	}

	if err := http2.ConfigureServer(&ctrl.server, nil); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (ctrl *Controller) Start() {
	go func() {
		if ctrl.server.TLSConfig.Certificates == nil {
			ctrl.logger.Info().Msgf("starting server at http://%s\n", ctrl.addr)
			if err := ctrl.server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
				ctrl.logger.Fatal().Msgf("server on '%s' ended: %v", ctrl.addr, err)
			}
		} else {
			ctrl.logger.Info().Msgf("starting server at https://%s\n", ctrl.addr)
			if err := ctrl.server.ListenAndServeTLS("", ""); !errors.Is(err, http.ErrServerClosed) {
				ctrl.logger.Fatal().Msgf("server on '%s' ended: %v", ctrl.addr, err)
			}
		}
	}()
}

func (ctrl *Controller) Stop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

func (ctrl *Controller) GracefulStop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

// getWikidataResource
// @Summary      Get service resource metadata
// @Description  Gets the JSON representation of the service resource with the configured properties.
// @Security 	 ApiKeyAuth
// @Tags         Wikidata
// @Produce      json
// @Param		 authority path string true "Authority type"
// @Param		 identifier path string true "Authority identifier"
// @Success      200  {object}  shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /{authority}/{identifier} [get]
func (ctrl *Controller) getWikidataResource(c *gin.Context) {
	var identifier string
	authority := c.Param("authority")
	identifier = c.Param("identifier")
	ctrl.logger.Info().Interface("authority", authority).Interface("identifier", identifier).Msgf("GET /api/v1/%s/%s", authority, identifier)
	var resource *shared.Resource
	var err error
	if authority != AuthorityParamWikidata {
		entry, hubErr := ctrl.hub.GetMatch(identifier, authority)
		if hubErr != nil {
			if errors.Is(hubErr, shared.NoConnection) {
				c.JSON(http.StatusOK, shared.NewErrorResource(
					identifier, NoWikidataConnectionError,
					fmt.Sprintf("No wikidata connection found for authority %s.", authority)))
			} else {
				shared.NewResultMessage(c, http.StatusInternalServerError,
					errors.Errorf("cannot match identifier %s with authority %s to wikidata. Error: %s.", identifier, authority, hubErr.Error()))
			}
			ctrl.logger.Err(hubErr).Interface("identifier", identifier).Interface("authority", authority).Msg("cannot match identifier with authority to wikidata")
			return
		}
		identifier = entry.WikidataIdentifier
	}
	if ctrl.useElastic {
		resource, err = ctrl.elastic.Get(identifier)
	} else {
		resource, err = ctrl.cache.Get(identifier)
	}
	if err != nil {
		if shared.IsResourceNotFoundError(err) {
			resource, err = ctrl.rest.Get(identifier)
			if err != nil {
				ctrl.logger.Err(err).Msgf("failed to retrieve wikidata item with id %s. %+v", identifier, err)
				shared.NewResultMessage(c, http.StatusInternalServerError,
					errors.Errorf("cannot find '%s' because of %s", identifier, err.Error()))
				return
			}
			if ctrl.useElastic {
				errRest := ctrl.elastic.IndexDocument(*resource)
				if errRest != nil {
					shared.NewResultMessage(c, http.StatusInternalServerError,
						errors.Errorf("cannot index '%s' because of %s",
							identifier, errRest.Error()))
					return
				}
			} else {
				b, errRest := json.Marshal(resource)
				if errRest != nil {
					shared.NewResultMessage(c, http.StatusInternalServerError,
						errors.Errorf("cannot marshal '%s' because of %s",
							identifier, errRest.Error()))
					return
				}
				errRest = ctrl.cache.Set(identifier, b)
				if errRest != nil {
					shared.NewResultMessage(c, http.StatusInternalServerError, errors.Errorf("error '%s' for idenitifer %s", errRest, identifier))
					return
				}
			}
			resource.OriginId = identifier
			c.JSON(http.StatusOK, resource)
			return
		} else {
			shared.NewResultMessage(c, http.StatusInternalServerError, errors.Errorf("error '%s' for idenitifer %s", err, identifier))
			return
		}
	}
	c.JSON(http.StatusOK, resource)
}

// getWikidataResourceFiltered
// @Summary      Get wikidata resource filtered for fields.
// @Description  Get json representation of a wikidata resources with just the fields listed.
// @Security 	 ApiKeyAuth
// @Tags         Wikidata
// @Produce      json
// @Param		 authority path string true "Authority type"
// @Param		 identifier path string true "Authority identifier"
// @Param        filter body shared.WikidataResourceFilter true "Filter for Wikidata resource"
// @Success      200  {object}  shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /{authority}/{identifier} [post]
func (ctrl *Controller) getWikidataResourceFiltered(c *gin.Context) {
	var identifier string
	authority := c.Param("authority")
	identifier = c.Param("identifier")
	var filter shared.WikidataResourceFilter
	if err := c.BindJSON(&filter); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, errors.Errorf("Could not parse request body: %s", err.Error()))
	}
	var resource *shared.Resource
	var err error
	if authority != AuthorityParamWikidata {
		entry, hubErr := ctrl.hub.GetMatch(identifier, authority)
		if hubErr != nil {
			if errors.Is(hubErr, shared.NoConnection) {
				c.JSON(http.StatusOK, shared.NewErrorResource(
					identifier, NoWikidataConnectionError, fmt.Sprintf("No wikidata connection for authority %s.", authority),
				))
			} else {
				shared.NewResultMessage(c, http.StatusInternalServerError,
					errors.Errorf("cannot match identifier %s with authority %s to wikidata. Error: %s.", identifier, authority, hubErr.Error()))
			}
			return
		}
		identifier = entry.WikidataIdentifier
	}
	if ctrl.useElastic {
		resource, err = ctrl.elastic.Get(identifier)
	} else {
		resource, err = ctrl.cache.Get(identifier)
	}
	if err != nil {
		if shared.IsResourceNotFoundError(err) {
			resource, err = ctrl.rest.Get(identifier)
			if err != nil {
				ctrl.logger.Err(err).Msgf("failed to retrieve wikidata item with id %s. %+v", identifier, err)
				shared.NewResultMessage(c, http.StatusInternalServerError,
					errors.Errorf("cannot find '%s' because of %s", identifier, err.Error()))
				return
			}
			if ctrl.useElastic {
				errRest := ctrl.elastic.IndexDocument(*resource)
				if errRest != nil {
					shared.NewResultMessage(c, http.StatusInternalServerError,
						errors.Errorf("cannot index '%s' because of %s",
							identifier, errRest.Error()))
					return
				}
			} else {
				b, errRest := json.Marshal(resource)
				if errRest != nil {
					shared.NewResultMessage(c, http.StatusInternalServerError,
						errors.Errorf("cannot marshal '%s' because of %s",
							identifier, errRest.Error()))
					return
				}
				errRest = ctrl.cache.Set(identifier, b)
				if errRest != nil {
					shared.NewResultMessage(c, http.StatusInternalServerError, errors.Errorf("error '%s' for idenitifer %s", errRest, identifier))
					return
				}
			}
		} else {
			shared.NewResultMessage(c, http.StatusInternalServerError, errors.Errorf("error '%s' for idenitifer %s", err, identifier))
			return
		}
	}

	filteredResource := &shared.Resource{
		ID:           resource.ID,
		Labels:       map[string]string{},
		Descriptions: map[string]string{},
		Data:         make(map[string][]*shared.Data),
		LastModified: resource.LastModified,
		OriginId:     identifier,
	}
	for _, lang := range filter.Languages {
		label, ok := resource.Labels[lang]
		if ok {
			filteredResource.Labels[lang] = label
		}
		description, ok := resource.Descriptions[lang]
		if ok {
			filteredResource.Descriptions[lang] = description
		}
	}

	for _, property := range filter.Properties {
		propertyValue, ok := resource.Data[property]
		if ok {
			filteredResource.Data[property] = propertyValue
		}
	}

	if filter.IncludeSiteLinks {
		filteredResource.SiteLinks = map[string]shared.SiteLink{}
		for key, link := range resource.SiteLinks {
			for _, lang := range filter.Languages {
				if strings.HasPrefix(key, lang) {
					filteredResource.SiteLinks[key] = link
				}
			}
		}
	}

	c.JSON(http.StatusOK, filteredResource)
}
