package service_marc

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/marc"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"strings"
)

var authorityPrefixes = map[string]string{
	"(DE-588)": "gnd",
	"(IDREF)":  "idref",
	"(IdRef)":  "idref",
	"(RERO)":   "rero",
	"(SBT11)":  "sbt",
}

func getWikidataResource(identifier string, client *WikidataServiceClient) (*shared.Resource, error) {
	resource, err := client.GetWikidataEntity(identifier)
	if err != nil {
		return nil, errors.Wrapf(err, "cannot get wikidata entity '%s'", identifier)
	}
	return resource, nil
}

func person(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	var id string
	for _, field := range rec.Datafields {
		if field.Tag == "035" {
			for _, sub := range field.Subfields {
				if sub.Code == "a" {
					id = sub.Text
				}
			}
		}
	}
	_ = id
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var personFields []*marc.Datafield

	field880List, ok := objects["880"]
	if !ok {
		field880List = nil
	}

	// all person relevant fields into personFields list
	field700, ok := objects["700"]
	if ok {
		for _, fld := range field700 {
			var doNotUse bool

			// 700 3# families are no persons
			if fld.Ind1 == "3" {
				doNotUse = true
			}

			// 700  $$t works are no persons
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				personFields = append(personFields, fld)
			}
		}
	}
	field100, ok := objects["100"]
	if ok {
		for _, fld := range field100 {
			// 100 3# families are no persons
			if fld.Ind1 != "3" {
				personFields = append(personFields, fld)
			}

		}
	}

	// iterate all person relevant fields
	for _, field := range personFields {
		var linkedField string
		var identifier string
		var authority string

		for _, subField := range field.Subfields {
			switch subField.Code {
			case "0":
				identifier = subField.Text
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}

		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field880Item := range field880List {
				var identifierField880 string
				var authorityField880 string
				var found = false
				for _, subField := range field880Item.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "100-") || strings.HasPrefix(subField.Text, "700-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field880Item.Subfields {
					switch subField.Code {
					case "0":
						identifierField880 = subField.Text
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifierField880, prefix) {
						authorityField880 = a
					}
				}
				if authorityField880 == "" {
					authorityField880 = "alternateRepresentation"
				}
				if authorityField880 == "gnd" && identifierField880 != "" {
					wikidataResource, err := getWikidataResource(identifierField880, client)
					if err != nil {
						allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
					} else {
						allResult = append(allResult, wikidataResource)
					}
				}
			}
		}
	}

	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}

func subjectPerson(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var subjectFields []*marc.Datafield
	field600, ok := objects["600"]
	if ok {
		for _, fld := range field600 {
			var doNotUse bool

			// 600 3# families are no persons
			if fld.Ind1 == "3" {
				doNotUse = true
			}

			// 600  $$t works are no persons
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "0":
				identifier = subField.Text
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
	}
	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}
