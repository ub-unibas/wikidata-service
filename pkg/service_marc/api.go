//go:generate swag init --parseDependency  --parseInternal -g ./api.go

package service_marc

import (
	"context"
	"crypto/tls"
	"emperror.dev/errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/je4/utils/v2/pkg/zLogger"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/marc"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/service_marc/docs"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"golang.org/x/net/http2"
	"net/http"
	"net/url"
	"strings"
)

const BasePath = "/api/v1"
const WikidataServiceErrorType = "WikidataServiceError"

type marc21Service interface {
	Do(*marc.QueryStruct) ([]any, error)
}

//	@title			MARC Structure Enrichment
//	@version		1.0
//	@description	Enriches MARC Structure with GND authority ID with Wikidata metadata
//	@termsOfService	http://swagger.io/terms/

//	@contact.name	University Library Basel, Informatik
//	@contact.url	https://ub.unibas.ch
//	@contact.email	it-ub@unibas.ch

//	@license.name	Apache 2.0
//	@license.url	http://www.apache.org/licenses/LICENSE-2.0.html

// @securityDefinitions.apikey	ApiKeyAuth
// @in							header
// @name						Authorization
// @description					Bearer Authentication with JWT

func NewController(addr, extAddr string,
	serverTlsConfig *tls.Config,
	serviceUrl string,
	languages, properties []string,
	includedSiteLinks bool,
	tlsConfig *tls.Config,
	logger *ublogger.Logger) (*Controller, error) {
	wikidataServiceClient, err := NewWikidataServiceClient(
		serviceUrl, languages, properties, includedSiteLinks, tlsConfig, *logger)
	if err != nil {
		return nil, err
	}

	u, err := url.Parse(extAddr)
	if err != nil {
		return nil, errors.Wrapf(err, "invalid external address '%s'", extAddr)
	}
	subPath := "/" + strings.Trim(u.Path, "/")
	if len(u.Port()) == 0 {
		docs.SwaggerInfo.Host = u.Hostname()
	} else {
		docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", u.Hostname(), u.Port())
	}
	docs.SwaggerInfo.BasePath = "/" + strings.Trim(subPath+BasePath, "/")
	if tlsConfig == nil && len(u.Port()) != 0 {
		docs.SwaggerInfo.Schemes = []string{"http"}
	} else {
		docs.SwaggerInfo.Schemes = []string{"https"}
	}
	router := gin.Default()

	c := &Controller{
		addr:                  addr,
		router:                router,
		subPath:               subPath,
		service:               make(map[string]marc21Service),
		logger:                *logger,
		WikidataServiceClient: wikidataServiceClient,
	}
	if initError := c.Init(serverTlsConfig); initError != nil {
		return nil, errors.Wrapf(err, "cannot initialize marc controller: %v", initError)
	}
	return c, nil
}

type Controller struct {
	server                http.Server
	router                *gin.Engine
	addr                  string
	subPath               string
	url                   *url.URL
	service               map[string]marc21Service
	logger                zLogger.ZLogger
	WikidataServiceClient *WikidataServiceClient
}

func (ctrl *Controller) Init(tlsConfig *tls.Config) error {
	var auth = func() gin.HandlerFunc {
		return func(c *gin.Context) {
			if len(c.GetHeader("Authorization")) == 0 {
				shared.NewResultMessage(c, http.StatusUnauthorized, errors.New("Authorization is required Header"))
				c.Abort()
			}
			c.Next()
		}
	}

	v1 := ctrl.router.Group(BasePath)
	//v1.Use(auth())
	_ = auth
	v1.POST("/person", ctrl.getPerson)
	v1.POST("/person/subject", ctrl.getPersonSubject)

	v1.POST("/family", ctrl.getFamily)
	v1.POST("/family/subject", ctrl.getFamilySubject)

	v1.POST("/conference", ctrl.getConference)
	v1.POST("/conference/subject", ctrl.getConferenceSubject)

	v1.POST("/corporation", ctrl.getCorporation)
	v1.POST("/corporation/subject", ctrl.getCorporationSubject)

	v1.POST("/geo", ctrl.getGeo)
	v1.POST("/geo/subject", ctrl.getGeoSubject)

	v1.POST("/title", ctrl.getTitle)
	v1.POST("/title/subject", ctrl.getTitleSubject)

	v1.POST("/topic", ctrl.getTopic)

	ctrl.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	ctrl.server = http.Server{
		Addr:      ctrl.addr,
		Handler:   ctrl.router,
		TLSConfig: tlsConfig,
	}
	if err := http2.ConfigureServer(&ctrl.server, nil); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (ctrl *Controller) Start() {
	go func() {
		var err error
		if ctrl.server.TLSConfig.Certificates == nil {
			ctrl.logger.Info().Msgf("starting server at http://%s\n", ctrl.addr)
			err = ctrl.server.ListenAndServe()
		} else {
			ctrl.logger.Info().Msgf("starting server at https://%s", ctrl.addr)
			err = ctrl.server.ListenAndServeTLS("", "")
		}
		if !errors.Is(err, http.ErrServerClosed) {
			ctrl.logger.Fatal().Msgf("server on '%s' ended with error: %v", ctrl.addr, err)
		}
	}()
}

func (ctrl *Controller) Stop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

func (ctrl *Controller) GracefulStop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

// getPerson
// @Summary      Get Wikidata metadata for a person.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     queryStruct body marc.QueryStruct true "Marc body structure"
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /person [POST]
func (ctrl *Controller) getPerson(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := person(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// getPerson
// @Summary      Get Wikidata metadata for a person as subject.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /person/subject [POST]
func (ctrl *Controller) getPersonSubject(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectPerson(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a family.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /family [POST]
func (ctrl *Controller) getFamily(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := family(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a family as a subject.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /family/subject [POST]
func (ctrl *Controller) getFamilySubject(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectFamily(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a geo location or place.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /geo [POST]
func (ctrl *Controller) getGeo(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := geo(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a geo location or place as a subject.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /geo/subject [POST]
func (ctrl *Controller) getGeoSubject(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectGeo(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a conference.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /conference [POST]
func (ctrl *Controller) getConference(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := conference(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a conference as a subject.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /conference/subject [POST]
func (ctrl *Controller) getConferenceSubject(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectConference(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a corporation.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /corporation [POST]
func (ctrl *Controller) getCorporation(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := corporation(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a corporation as a subject.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /corporation/subject [POST]
func (ctrl *Controller) getCorporationSubject(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectCorporation(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a title.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /title [POST]
func (ctrl *Controller) getTitle(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := title(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a title as a subject.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /title/subject [POST]
func (ctrl *Controller) getTitleSubject(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := subjectTitle(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}

// @Summary      Get Wikidata metadata for a topic.
// @Description  Using the supplied MARC data fields, extracts the GND identifiers and returns Wikidata metadata.
// @Security 	 ApiKeyAuth
// @Tags         GND
// @Produce      json
// @Param	     MarcQueryStruct	body		marc.QueryStruct	true	"Get "
// @Success      200  {object}  []shared.Resource
// @Failure      400  {object}  shared.HTTPResultMessage
// @Failure      404  {object}  shared.HTTPResultMessage
// @Failure      500  {object}  shared.HTTPResultMessage
// @Router       /topic [POST]
func (ctrl *Controller) getTopic(c *gin.Context) {
	var requestBody = &marc.QueryStruct{}
	if err := c.BindJSON(requestBody); err != nil {
		shared.NewResultMessage(c, http.StatusBadRequest, err)
		return
	}
	result, err := topic(requestBody, ctrl.WikidataServiceClient)
	if err != nil {
		shared.NewResultMessage(c, http.StatusInternalServerError, err)
		return
	}
	if c.Query("format") == "xml" {
		c.XML(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, result)
}
