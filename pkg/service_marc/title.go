package service_marc

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/marc"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"strings"
)

func title(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var titleFields []*marc.Datafield
	field880List, ok := objects["880"]
	if !ok {
		field880List = nil
	}
	field700, ok := objects["700"]
	if ok {
		titleFields = append(titleFields, field700...)
	}
	field710, ok := objects["710"]
	if ok {
		titleFields = append(titleFields, field710...)
	}
	field711, ok := objects["711"]
	if ok {
		titleFields = append(titleFields, field711...)
	}
	field130, ok := objects["130"]
	if ok {
		titleFields = append(titleFields, field130...)
	}
	field730, ok := objects["730"]
	if ok {
		titleFields = append(titleFields, field730...)
	}
	for _, field := range titleFields {
		var linkedField string
		var identifier string
		var authority string
		var ok bool
		for _, subField := range field.Subfields {
			if field.Tag == "130" {
				ok = true
				break
			}
			if field.Tag == "700" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "710" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "711" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "730" {
				ok = true
				break
			}
		}

		if ok {
			for _, subField := range field.Subfields {
				switch field.Tag {
				case "130", "730":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				case "700":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				case "710":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				case "711":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				}
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field880Item := range field880List {
				var identifierField880 string
				var authorityField880 string
				var found = false
				for _, subField := range field880Item.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "130-") || strings.HasPrefix(subField.Text, "700-") || strings.HasPrefix(subField.Text, "710-") || strings.HasPrefix(subField.Text, "730-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field880Item.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "130-") || strings.HasPrefix(subField.Text, "730-") {
							switch subField.Code {
							case "0":
								identifierField880 = subField.Text
							}
						}
						if strings.HasPrefix(subField.Text, "700-") {
							switch subField.Code {
							case "0":
								identifierField880 = subField.Text
							}
						}
						if strings.HasPrefix(subField.Text, "710-") {
							switch subField.Code {
							case "0":
								identifierField880 = subField.Text
							}
						}
						if strings.HasPrefix(subField.Text, "711-") {
							switch subField.Code {
							case "0":
								identifierField880 = subField.Text
							}
						}
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifierField880, prefix) {
						authorityField880 = a
					}
				}
				if authorityField880 == "" {
					authorityField880 = "alternateRepresentation"
				}
				if authorityField880 == "gnd" && identifierField880 != "" {
					var err error
					wikidataResource, err := getWikidataResource(identifierField880, client)
					if err != nil {
						allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
					} else {
						allResult = append(allResult, wikidataResource)
					}
				}
			}
		}
	}
	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}

func subjectTitle(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var subjectFields []*marc.Datafield
	field600, ok := objects["600"]
	if ok {
		subjectFields = append(subjectFields, field600...)
	}
	field610, ok := objects["610"]
	if ok {
		subjectFields = append(subjectFields, field610...)
	}
	field611, ok := objects["611"]
	if ok {
		subjectFields = append(subjectFields, field611...)
	}
	field630, ok := objects["630"]
	if ok {
		subjectFields = append(subjectFields, field630...)
	}
	for _, field := range subjectFields {
		var identifier string
		var authority string
		var ok bool
		for _, subField := range field.Subfields {
			if field.Tag == "600" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "610" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "611" && subField.Code == "t" {
				ok = true
				break
			}
			if field.Tag == "630" {
				ok = true
				break
			}
		}
		if ok {
			for _, subField := range field.Subfields {
				switch field.Tag {
				case "630":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				case "600":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				case "610":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				case "611":
					switch subField.Code {
					case "0":
						identifier = subField.Text
					}
				}
				switch field.Ind2 {
				case "0":
					authority = "lcsh"
				case "2":
					authority = "mesh"
				case "7":
					for _, subfield := range field.Subfields {
						if subfield.Code == "2" {
							switch subfield.Text {
							case "gnd":
								authority = "gnd"
							case "idsbb":
								authority = "idsbb"
							case "idref":
								authority = "idref"
							}
						}
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
	}

	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}
