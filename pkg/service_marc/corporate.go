package service_marc

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/marc"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"strings"
)

func corporation(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var fields []*marc.Datafield
	field880List, ok := objects["880"]
	if !ok {
		field880List = nil
	}
	field710, ok := objects["710"]
	if ok {
		for _, fld := range field710 {
			var doNotUse bool

			// 710  $$t works are no corporates
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				fields = append(fields, fld)
			}
		}
	}
	field110, ok := objects["110"]
	if ok {
		fields = append(fields, field110...)
	}
	for _, field := range fields {
		var linkedField string
		var name string
		var description []string
		var role []string
		var identifier string
		var authority string

		for _, subField := range field.Subfields {
			switch subField.Code {
			case "a":
				name = subField.Text
			case "b":
				name += ", " + subField.Text
			case "g":
				description = append(description, subField.Text)
			case "0":
				identifier = subField.Text
			case "4":
				role = append(role, subField.Text)
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field880Item := range field880List {
				var identifierField880 string
				var authorityField880 string
				var found = false
				for _, subField := range field880Item.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "110-") || strings.HasPrefix(subField.Text, "710-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field880Item.Subfields {
					switch subField.Code {
					case "a":
						name = subField.Text
					case "b":
						name += ", " + subField.Text
					case "g":
						description = append(description, subField.Text)
					case "0":
						identifierField880 = subField.Text
					case "4":
						role = append(role, subField.Text)
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifierField880, prefix) {
						authorityField880 = a
					}
				}
				if authorityField880 == "" {
					authorityField880 = "alternateRepresentation"
				}
				if authorityField880 == "gnd" && identifierField880 != "" {
					var err error
					wikidataResource, err := getWikidataResource(identifierField880, client)
					if err != nil {
						allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
					} else {
						allResult = append(allResult, wikidataResource)
					}
				}
			}
		}
	}
	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}

func subjectCorporation(record *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if record == nil {
		return nil, errors.New("empty query struct")
	}
	if record.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range record.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var subjectFields []*marc.Datafield
	field610, ok := objects["610"]
	if ok {
		for _, fld := range field610 {
			var doNotUse bool

			// 610  $$t works are no corporates
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "0":
				identifier = subField.Text
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
	}
	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}
