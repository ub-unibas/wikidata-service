package service_marc

import (
	"crypto/tls"
	"emperror.dev/errors"
	"encoding/json"
	"github.com/je4/utils/v2/pkg/zLogger"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"io"
	"net/http"
	"net/url"
	"strings"
)

var errorNotFound = errors.New("not found")
var errorInternal = errors.New("internal error")
var errorBadRequest = errors.New("bad request")
var errorNoService = errors.New("no service")
var errorUnknown = errors.New("unknown error")
var errorServiceUnavailable = errors.New("service unavailable")

type WikidataServiceClient struct {
	httpClient *http.Client
	serviceUrl *url.URL
	filter     *shared.WikidataResourceFilter
	logger     zLogger.ZLogger
}

func NewWikidataServiceClient(serviceUrl string, languages, properties []string, includeSiteLinks bool, tlsConfig *tls.Config, logger zLogger.ZLogger) (*WikidataServiceClient, error) {
	u, err := url.Parse(serviceUrl)
	if err != nil {
		return nil, err
	}
	return &WikidataServiceClient{
		httpClient: shared.NewServiceHttpClient(tlsConfig),
		serviceUrl: u,
		filter: &shared.WikidataResourceFilter{
			Languages:        languages,
			Properties:       properties,
			IncludeSiteLinks: includeSiteLinks,
		},
		logger: logger,
	}, nil
}

func (c WikidataServiceClient) GetWikidataEntity(identifier string) (*shared.Resource, error) {
	if strings.HasPrefix(identifier, "(DE-588)") {
		identifier = strings.TrimPrefix(identifier, "(DE-588)")
	}

	idUrl := c.serviceUrl.JoinPath("gnd", identifier)

	reader, err := c.filter.ToReader()
	if err != nil {
		return nil, err
	}

	res, err := c.httpClient.Post(idUrl.String(), "application/json", reader)
	if err != nil {
		return nil, errors.Wrapf(err, "Could not get wikidata resource from wikidata service for identifier '%s'", identifier)
	}
	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {

		}
	}(res.Body)
	if res.StatusCode == http.StatusNotFound {
		return nil, errorNotFound
	}
	if res.StatusCode == http.StatusInternalServerError {
		return nil, errors.Wrapf(errorInternal, "internal server error gnd '%s': %s", identifier, res.Status)
	}
	if res.StatusCode == http.StatusBadRequest {
		return nil, errors.Wrapf(errorBadRequest, "bad request gnd '%s': %s", identifier, res.Status)
	}
	if res.StatusCode != http.StatusOK {
		return nil, errors.Wrapf(errorUnknown, "unknown error gnd '%s'", identifier)
	}
	var result = &shared.Resource{}
	dec := json.NewDecoder(res.Body)
	if err = dec.Decode(result); err != nil {
		return nil, errors.Wrapf(err, "cannot decode result json of '%s'", identifier)
	}

	return result, nil
}
