package service_marc

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/marc"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"strings"
)

func conference(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var conferenceFields []*marc.Datafield
	field880List, ok := objects["880"]
	if !ok {
		field880List = nil
	}
	field711, ok := objects["711"]
	if ok {
		for _, fld := range field711 {
			var doNotUse bool

			// 711  $$t works are no conferences
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				conferenceFields = append(conferenceFields, fld)
			}
		}
	}
	field111, ok := objects["111"]
	if ok {
		conferenceFields = append(conferenceFields, field111...)
	}
	for _, field := range conferenceFields {
		var linkedField string
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "0":
				identifier = subField.Text
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := client.GetWikidataEntity(identifier)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, dataField880 := range field880List {
				var identifierField880 string
				var authorityField880 string
				var found = false
				for _, subField := range dataField880.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "111-") || strings.HasPrefix(subField.Text, "711-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range dataField880.Subfields {
					switch subField.Code {
					case "0":
						identifierField880 = subField.Text
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifierField880, prefix) {
						authorityField880 = a
					}
				}
				if authorityField880 == "" {
					authorityField880 = "alternateRepresentation"
				}
				if authorityField880 == "gnd" && identifierField880 != "" {
					var err error
					wikidataResource, err := getWikidataResource(identifierField880, client)
					if err != nil {
						allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
					} else {
						allResult = append(allResult, wikidataResource)
					}
				}
			}
		}
	}
	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}

func subjectConference(rec *marc.QueryStruct, wikidataServiceClient *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var subjectFields []*marc.Datafield
	field611, ok := objects["611"]
	if ok {
		for _, fld := range field611 {
			var doNotUse bool

			// 611  $$t works are no conferences
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "0":
				identifier = subField.Text
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		if authority == "wikidataServiceClient" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, wikidataServiceClient)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
	}
	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}
