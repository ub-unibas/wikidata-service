package service_marc

import (
	"emperror.dev/errors"
	"fmt"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/marc"
	"gitlab.switch.ch/ub-unibas/wikidata-service/pkg/shared"
	"strings"
)

func family(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var fields []*marc.Datafield
	field880List, ok := objects["880"]
	if !ok {
		field880List = nil
	}
	field700, ok := objects["700"]
	if ok {
		for _, fld := range field700 {
			var doNotUse bool

			// only 700 3# are families
			if fld.Ind1 != "3" {
				doNotUse = true
			}

			// 700  $$t works are no families
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				fields = append(fields, fld)
			}
		}
	}
	field100, ok := objects["100"]
	if ok {
		for _, fld := range field100 {
			// only 100 3# are families
			if fld.Ind1 == "3" {
				fields = append(fields, fld)
			}
		}
	}
	for _, field := range fields {
		var linkedField string
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "0":
				identifier = subField.Text
			case "6":
				linkedField = subField.Text
			}
		}
		for prefix, a := range authorityPrefixes {
			if strings.HasPrefix(identifier, prefix) {
				authority = a
			}
		}
		if authority == "" {
			authority = "unknown"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
		if strings.HasPrefix(linkedField, "880-") {
			linkVal := strings.TrimPrefix(linkedField, "880-")
			for _, field880Item := range field880List {
				var identifierField880 string
				var authorityField880 string
				var found = false
				for _, subField := range field880Item.Subfields {
					if subField.Code == "6" {
						if strings.HasPrefix(subField.Text, "100-") || strings.HasPrefix(subField.Text, "700-") {
							if subField.Text[4:6] == linkVal {
								found = true
							}
						}
					}
				}
				if !found {
					continue
				}
				for _, subField := range field880Item.Subfields {
					switch subField.Code {
					case "0":
						identifierField880 = subField.Text
					}
				}
				for prefix, a := range authorityPrefixes {
					if strings.HasPrefix(identifierField880, prefix) {
						authorityField880 = a
					}
				}
				if authorityField880 == "" {
					authorityField880 = "alternateRepresentation"
				}
				if authorityField880 == "gnd" && identifierField880 != "" {
					var err error
					wikidataResource, err := getWikidataResource(identifierField880, client)
					if err != nil {
						allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
					} else {
						allResult = append(allResult, wikidataResource)
					}
				}
			}
		}
	}
	return allResult, nil
}

func subjectFamily(rec *marc.QueryStruct, client *WikidataServiceClient) ([]*shared.Resource, error) {
	if rec == nil {
		return nil, errors.New("empty query struct")
	}
	if rec.Datafields == nil {
		return nil, errors.New("no datafield in query struct")
	}
	var objects = map[string][]*marc.Datafield{}
	for _, object := range rec.Datafields {
		if object == nil {
			return nil, errors.New("nil datafield in query struct")
		}

		if _, ok := objects[object.Tag]; !ok {
			objects[object.Tag] = []*marc.Datafield{}
		}
		objects[object.Tag] = append(objects[object.Tag], object)
	}

	var allResult []*shared.Resource
	var subjectFields []*marc.Datafield
	field600, ok := objects["600"]
	if ok {
		for _, fld := range field600 {
			var doNotUse bool

			// only 600 3# are families
			if fld.Ind1 != "3" {
				doNotUse = true
			}

			// 600  $$t works are no families
			for _, sub := range fld.Subfields {
				if sub.Code == "t" {
					doNotUse = true
					break
				}
			}
			if !doNotUse {
				subjectFields = append(subjectFields, fld)
			}
		}
	}
	for _, field := range subjectFields {
		var identifier string
		var authority string
		for _, subField := range field.Subfields {
			switch subField.Code {
			case "0":
				identifier = subField.Text
			}
		}
		switch field.Ind2 {
		case "0":
			authority = "lcsh"
		case "2":
			authority = "mesh"
		case "7":
			for _, subfield := range field.Subfields {
				if subfield.Code == "2" {
					switch subfield.Text {
					case "gnd":
						authority = "gnd"
					case "idsbb":
						authority = "idsbb"
					case "idref":
						authority = "idref"
					}
				}
			}
		}
		if authority == "" {
			authority = "other"
		}
		if authority == "gnd" && identifier != "" {
			var err error
			wikidataResource, err := getWikidataResource(identifier, client)
			if err != nil {
				allResult = append(allResult, shared.NewErrorResource(identifier, WikidataServiceErrorType, fmt.Sprintf("Failed: %s", err)))
			} else {
				allResult = append(allResult, wikidataResource)
			}
		}
	}
	if allResult == nil {
		return []*shared.Resource{}, nil
	}
	return allResult, nil
}
