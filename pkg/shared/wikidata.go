package shared

import (
	"emperror.dev/errors"
	"encoding/json"
	"fmt"
	"github.com/je4/utils/v2/pkg/zLogger"
	"io"
	"net/http"
	"strings"
)

const WikidataUrl = "https://www.wikidata.org/w/rest.php/wikibase/v1"
const ItemPath = "/entities/items"
const PropertyPath = "/entities/properties"
const ParamName = "_fields"
const CommonsUrl = "https://commons.wikimedia.org/wiki/File:"
const SpaceReplacement = "_"
const HeaderModifiedSince = "If-Modified-Since"
const ResponseHeaderLastModified = "last-modified"
const HeaderAuthorization = "Authorization"

type WikidataRest struct {
	url               string
	propertyUrl       string
	param             string
	client            *http.Client
	metadataLanguages []string
	properties        []string
	logger            zLogger.ZLogger
}

func NewWikidataRestClient(metadataLanguages []string,
	properties []string,
	fields string,
	httpClient *http.Client,
	logger zLogger.ZLogger) *WikidataRest {
	return &WikidataRest{
		url:               WikidataUrl + ItemPath,
		propertyUrl:       WikidataUrl + PropertyPath,
		param:             ParamName + "=" + fields,
		client:            httpClient,
		metadataLanguages: metadataLanguages,
		properties:        properties,
		logger:            logger,
	}
}

func (w *WikidataRest) Get(itemId string) (*Resource, error) {
	url := w.url + "/" + itemId + "?" + w.param
	w.logger.Debug().Msgf("GET %s", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	response, err := w.client.Do(req)
	if err != nil {
		w.logger.Error().Msgf("Failed to retrieve wikidata item with id %s with error %v.", itemId, err)
		return nil, err
	}
	switch response.StatusCode {
	case http.StatusOK:
		wikidataItem, responseParseError := w.parseResponse(response)
		if responseParseError != nil {
			return nil, responseParseError
		}
		return &Resource{
			ID:           wikidataItem.ID,
			Labels:       w.extractLabels(wikidataItem),
			Descriptions: w.extractDescriptions(wikidataItem),
			Data:         w.extractProperties(wikidataItem),
			SiteLinks:    w.extractSiteLinks(wikidataItem),
			LastModified: response.Header.Get(ResponseHeaderLastModified),
		}, nil
	case http.StatusNotModified:
		return nil, errors.New("not modified")
	case http.StatusNotFound:
		return nil, errors.New("resource not found: " + itemId)
	default:
		return nil, errors.New("unexpected status code: " + response.Status)
	}
}

func (w *WikidataRest) GetLastModified(itemId string) (string, error) {
	url := w.url + "/" + itemId + "?" + w.param
	w.logger.Debug().Msgf("GET %s", url)
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	response, err := w.client.Do(request)
	if err != nil {
		w.logger.Error().Msgf("Failed to retrieve wikidata item with id %s with error %v.", itemId, err)
		return "", err
	}
	if response.StatusCode != http.StatusOK {
		return "", fmt.Errorf("request to Wikidata failed with status code %d", response.StatusCode)
	}
	lastModified := response.Header.Get("Last-Modified")
	return lastModified, nil
}

func (w *WikidataRest) parseResponse(response *http.Response) (*WikidataItem, error) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(response.Body)

	content, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var wikidataItem WikidataItem
	err = json.Unmarshal(content, &wikidataItem)
	if err != nil {
		return nil, err
	}

	return &wikidataItem, nil
}

func (w *WikidataRest) extractLabels(item *WikidataItem) map[string]string {
	labels := map[string]string{}
	for _, language := range w.metadataLanguages {
		if label, ok := item.Labels[language]; ok {
			labels[language] = label
		} else {
			w.logger.Debug().Msgf("label for language '%s' not found on item %s", language, item.ID)
		}
	}
	return labels
}

func (w *WikidataRest) extractDescriptions(item *WikidataItem) map[string]string {
	descriptions := map[string]string{}
	for _, language := range w.metadataLanguages {
		if description, ok := item.Descriptions[language]; ok {
			descriptions[language] = description
		} else {
			w.logger.Debug().Msgf("description for language '%s' not found on item %s", language, item.ID)
		}
	}
	return descriptions
}

func (w *WikidataRest) extractProperties(item *WikidataItem) map[string][]*Data {
	properties := map[string][]*Data{}
label:
	for statementKey, statementItem := range item.Statements {
		if len(w.properties) > 0 {
			for _, allowedProp := range w.properties {
				if statementKey != allowedProp {
					continue label
				}
			}
		}
		for _, statement := range statementItem {

			switch statement.Property.DataType {
			case "commonsMedia", "geo-shape", "tabular-data":
				if _, ok := properties[statementKey]; !ok {
					properties[statementKey] = []*Data{}
				}
				value, ok := statement.Value.Content.(string)
				if ok {
					properties[statementKey] = append(properties[statementKey], &Data{
						Simple: CommonsUrl + strings.Replace(value, " ", SpaceReplacement, -1),
					})
				} else {
					w.logger.Error().Msgf("Could not parse value for type %s with content %s for property %s.", statement.Value.Type, statement.Value.Content, statement.Property.ID)
				}
				break
			case "globe-coordinate":
				if _, ok := properties[statementKey]; !ok {
					properties[statementKey] = []*Data{}
				}
				properties[statementKey] = append(properties[statementKey], &Data{
					GlobeCoordinate: statement.Value.GetGlobeCoordinate(),
				})
				break
			case "monolingualtext":
				if _, ok := properties[statementKey]; !ok {
					properties[statementKey] = []*Data{}
				}
				properties[statementKey] = append(properties[statementKey], &Data{
					MonoLingualText: statement.Value.GetMonolingualText(),
				})
				break
			case "quantity":
				if _, ok := properties[statementKey]; !ok {
					properties[statementKey] = []*Data{}
				}
				properties[statementKey] = append(properties[statementKey], &Data{
					Quantity: statement.Value.GetQuantity(),
				})
				break
			case "time":
				if _, ok := properties[statementKey]; !ok {
					properties[statementKey] = []*Data{}
				}
				properties[statementKey] = append(properties[statementKey], &Data{
					Time: statement.Value.GetTime(),
				})
				break
			case "wikibase-item", "wikibase-property", "external-id", "url", "string", "musicalnote", "mathematical-expression":
				if _, ok := properties[statementKey]; !ok {
					properties[statementKey] = []*Data{}
				}
				value, ok := statement.Value.Content.(string)
				if !ok {
					w.logger.Info().Msgf("Content type %s has not a string as value %s for property %s.", statement.Value.Type, statement.Value.Content, statementKey)
				}
				properties[statementKey] = append(properties[statementKey], &Data{
					Simple: checkOk(value, ok),
				})
				break
			default:
				w.logger.Error().Msgf("Found type that is not handled: %s", statement.Property.DataType)
			}
		}
	}
	return properties
}

func (w *WikidataRest) extractSiteLinks(item *WikidataItem) map[string]SiteLink {
	return item.SiteLinks
}

func (w *WikidataRest) GetPropertyName(id string) (string, error) {
	url := w.propertyUrl + "/" + id
	w.logger.Debug().Msgf("GET %s", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	response, err := w.client.Do(req)
	if err != nil {
		w.logger.Error().Msgf("Failed to retrieve wikidata item with id %s with error %v.", id, err)
		return "", err
	}
	if response.StatusCode != http.StatusOK {
		return "", errors.New("unexpected status code: " + response.Status)
	} else {
		defer func(Body io.ReadCloser) {
			e := Body.Close()
			if e != nil {

			}
		}(response.Body)

		content, err := io.ReadAll(response.Body)
		if err != nil {
			return "", err
		}

		var wikidataItem WikidataItem
		err = json.Unmarshal(content, &wikidataItem)
		if err != nil {
			return "", err
		}

		return wikidataItem.Labels["en"], nil
	}
}
