package shared

import (
	"os"
	"testing"
)

func TestParseLobidResponse(t *testing.T) {
	data, err := os.ReadFile("test_data/test.json")
	if err != nil {
		return
	}

	lobidResponse, err := ParseLobidResponse(data)
	if err != nil {
		t.Errorf("Error parsing resource %v", err)
	}
	size := len(lobidResponse.SameAs)
	if size != 11 {
		t.Errorf("Expected to have 11 entries in SameAs, but only got %d.", size)
	}

	wikidataId, err := lobidResponse.GetWikidataId()
	if err != nil {
		t.Errorf("Error getting wikidata id: %v", err)
	}
	if wikidataId != "Q42" {
		t.Errorf("Not the correct wikidata id: %s", wikidataId)
	}
}
