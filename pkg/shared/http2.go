package shared

import (
	"crypto/tls"
	"golang.org/x/net/http2"
	"net/http"
)

func NewServiceHttpClient(tlsConfig *tls.Config) *http.Client {
	return &http.Client{Transport: &http2.Transport{
		DialTLSContext:             nil,
		TLSClientConfig:            tlsConfig,
		ConnPool:                   nil,
		DisableCompression:         false,
		AllowHTTP:                  false,
		MaxHeaderListSize:          0,
		MaxReadFrameSize:           0,
		MaxDecoderHeaderTableSize:  0,
		MaxEncoderHeaderTableSize:  0,
		StrictMaxConcurrentStreams: false,
		ReadIdleTimeout:            0,
		PingTimeout:                0,
		WriteByteTimeout:           0,
		CountError:                 nil,
	}}
}
