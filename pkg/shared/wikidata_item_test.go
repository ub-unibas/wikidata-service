package shared

import (
	"encoding/json"
	"os"
	"path/filepath"
	"runtime"
	"testing"
)

func TestWikidataItemParsing(t *testing.T) {
	_, currentFile, _, _ := runtime.Caller(0)
	testdataPath := filepath.Join(filepath.Dir(currentFile), "test_data", "q42.json")
	jsonData, err := os.ReadFile(testdataPath)
	if err != nil {
		t.Errorf("Error reading test data file: %v", err)
		return
	}

	var wikidataItem WikidataItem
	err = json.Unmarshal(jsonData, &wikidataItem)
	if err != nil {
		t.Errorf("Error parsing JSON: %v", err)
	}

	expectedID := "Q42"
	if wikidataItem.ID != expectedID {
		t.Errorf("Expected ID: %s, Got: %s", expectedID, wikidataItem.ID)
	}

	expectedLabel := "Douglas Adams"
	if wikidataItem.Labels["en"] != expectedLabel {
		t.Errorf("Expected Label: %s, Got: %s", expectedLabel, wikidataItem.Labels["en"])
	}

	statement := wikidataItem.Statements["P31"]
	expectedStatementID := "Q42$F078E5B3-F9A8-480E-B7AC-D97778CBBEF9"
	if statement[0].ID != expectedStatementID {
		t.Errorf("Expected Statement ID: %s, Got: %s", expectedStatementID, statement[0].ID)
	}
}
