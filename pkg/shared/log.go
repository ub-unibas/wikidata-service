package shared

import (
	"github.com/rs/zerolog"
	"io"
	"log"
	"os"
	"strings"
)

func DefineLogger(logFile string, logLevel string) (zerolog.Logger, error) {
	var out io.Writer = os.Stdout
	if logFile != "" {
		fp, fileError := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
		if fileError != nil {
			log.Fatalf("cannot open logfile %s: %v", logFile, fileError)
		}
		out = fp
	}
	_logger := zerolog.New(out).With().Timestamp().Logger()
	switch strings.ToUpper(logLevel) {
	case "DEBUG":
		_logger = _logger.Level(zerolog.DebugLevel)
	case "INFO":
		_logger = _logger.Level(zerolog.InfoLevel)
	case "WARN":
		_logger = _logger.Level(zerolog.WarnLevel)
	case "ERROR":
		_logger = _logger.Level(zerolog.ErrorLevel)
	case "FATAL":
		_logger = _logger.Level(zerolog.FatalLevel)
	case "PANIC":
		_logger = _logger.Level(zerolog.PanicLevel)
	default:
		_logger = _logger.Level(zerolog.DebugLevel)
	}
	return _logger, nil
}
