package shared

import (
	"context"
	"emperror.dev/errors"
	"encoding/json"
	elasticsearch8 "github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
	"github.com/elastic/go-elasticsearch/v8/typedapi/core/search"
	"github.com/elastic/go-elasticsearch/v8/typedapi/types"
	"github.com/elastic/go-elasticsearch/v8/typedapi/types/enums/sortorder"
	"github.com/je4/utils/v2/pkg/zLogger"
	"os"
	"strings"
)

type Elastic struct {
	Client             *elasticsearch8.TypedClient
	WikidataRestClient *WikidataRest
	Index              string
	logger             zLogger.ZLogger
}

func EstablishNewClient(nodes []string, index, apikey, mappingFilePath string, caCert []byte, clearCache bool, logger zLogger.ZLogger) (*Elastic, error) {
	es, err := elasticsearch8.NewTypedClient(elasticsearch8.Config{
		Addresses: nodes,
		APIKey:    apikey,
		CACert:    caCert,
	})
	if err != nil {
		return nil, err
	}

	value, err := es.Indices.Exists(index).Do(context.Background())
	if err != nil {
		return nil, err
	}

	if !value {
		mapping, mappingError := os.ReadFile(mappingFilePath)
		if mappingError != nil {
			return nil, mappingError
		}
		indexReq := esapi.IndicesCreateRequest{
			Index: index,
			Body:  strings.NewReader(string(mapping)),
		}
		_, errCreate := indexReq.Do(context.Background(), es)
		if errCreate != nil {
			return nil, errCreate
		}
	} else if value && clearCache {
		_, errDelete := es.Indices.Delete(index).Do(context.Background())
		if errDelete != nil {
			return nil, errDelete
		}

		mapping, mappingError := os.ReadFile(mappingFilePath)
		if mappingError != nil {
			return nil, mappingError
		}
		indexReq := esapi.IndicesCreateRequest{
			Index: index,
			Body:  strings.NewReader(string(mapping)),
		}
		_, errCreate := indexReq.Do(context.Background(), es)
		if errCreate != nil {
			return nil, errCreate
		}
	}

	return &Elastic{
		Client: es,
		Index:  index,
		logger: logger,
	}, nil
}

func EstablishNewClientNoNewIndex(nodes []string, index, apikey string, caCert []byte, logger zLogger.ZLogger) (*Elastic, error) {
	es, err := elasticsearch8.NewTypedClient(elasticsearch8.Config{
		Addresses: nodes,
		APIKey:    apikey,
		CACert:    caCert,
	})
	if err != nil {
		return nil, err
	}

	return &Elastic{
		Client: es,
		Index:  index,
		logger: logger,
	}, nil
}

func (e Elastic) Get(id string) (*Resource, error) {
	response, err := e.Client.Get(e.Index, id).Do(context.Background())
	if err != nil {
		return nil, err
	}
	if response.Found {
		data, marshalError := response.Source_.MarshalJSON()
		if marshalError != nil {
			return nil, marshalError
		}
		if data == nil {
			return nil, errors.Errorf("Could not get data from source. %v", response.Source_)
		}
		var resource = &Resource{}
		unmarshalError := json.Unmarshal(data, resource)
		if unmarshalError != nil {
			return nil, unmarshalError
		}
		return resource, nil
	} else {
		return nil, ResourceNotFoundError{Identifier: id}
	}
}

func (e Elastic) IndexDocument(document Resource) error {
	_, err := e.Client.Index(e.Index).
		Id(document.ID).
		Document(document).
		Do(context.Background())
	if err != nil {
		return err
	}
	return nil
}

func (e Elastic) GetHubEntry(identifier string) (*IdHubEntry, error) {
	response, err := e.Client.Get(e.Index, identifier).Do(context.Background())
	if err != nil {
		return nil, err
	}
	if response.Found {
		data, marshalError := response.Source_.MarshalJSON()
		if marshalError != nil {
			return nil, marshalError
		}
		var idHubEntry *IdHubEntry
		unmarshalError := json.Unmarshal(data, idHubEntry)
		if unmarshalError != nil {
			return nil, unmarshalError
		}
		return idHubEntry, nil
	} else {
		return nil, ResourceNotFoundError{Identifier: identifier}
	}
}

func (e Elastic) IndexHubEntry(hubEntry *IdHubEntry) error {
	_, err := e.Client.Index(e.Index).
		Id(hubEntry.GNDIdentifier).
		Document(hubEntry).
		Do(context.Background())
	if err != nil {
		return err
	}
	return nil
}

func (e Elastic) ScrollIndex() (map[string]int, error) {
	totalVisited := 0
	size := 10000
	keyCounts := make(map[string]int)
	var sortValues []types.FieldValue
	sortCombinationsSlice := []types.SortCombinations{
		types.SortOptions{SortOptions: map[string]types.FieldSort{
			"_score": {Order: &sortorder.Asc},
		}}}
	sortValues = nil
	pointInTimeResponse, errPit := e.Client.OpenPointInTime(e.Index).KeepAlive("5m").Do(context.Background())
	if errPit != nil {
		return nil, errors.Wrapf(errPit, "open point in time failed: %v", errPit)
	}
	pointInTime := pointInTimeResponse.Id
	for {
		var err error
		var response *search.Response
		if sortValues != nil {
			response, err = e.Client.Search().
				Request(&search.Request{
					Query:       &types.Query{MatchAll: &types.MatchAllQuery{}},
					Sort:        sortCombinationsSlice,
					Size:        &size,
					SearchAfter: sortValues,
					Pit:         &types.PointInTimeReference{Id: pointInTime, KeepAlive: "5m"},
				}).
				Do(context.Background())
		} else {
			response, err = e.Client.Search().
				Request(&search.Request{
					Query: &types.Query{MatchAll: &types.MatchAllQuery{}},
					Sort:  sortCombinationsSlice,
					Size:  &size,
					Pit:   &types.PointInTimeReference{Id: pointInTime, KeepAlive: "5m"},
				}).
				Do(context.Background())
		}
		if err != nil {
			return nil, errors.Wrapf(err, "search request failed: %v", err)
		}
		hits := response.Hits
		if len(hits.Hits) <= 0 {
			return keyCounts, nil
		}
		for _, hit := range hits.Hits {
			var resource Resource
			unmarshalError := json.Unmarshal(hit.Source_, &resource)
			if unmarshalError != nil {
				return nil, errors.Wrapf(unmarshalError, "failed to unmarshal hit source: %v", unmarshalError)
			}

			if resource.Data != nil {
				for key := range resource.Data {
					keyCounts[key]++
				}
			}
			totalVisited++
			// use the last element to start the next search with search_after
			sortValues = hit.Sort
		}
		e.logger.Info().Msgf("Visited %d resources.", totalVisited)
		pointInTime = *response.PitId
	}
}
