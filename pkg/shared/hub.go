package shared

import (
	"emperror.dev/errors"
	"fmt"
	"github.com/je4/utils/v2/pkg/zLogger"
	"io"
	"net/http"
)

type IdHubController struct {
	cache        *IdHubCache
	useElastic   bool
	elastic      *Elastic
	lobidBaseUrl string
	logger       zLogger.ZLogger
	client       *http.Client
}

func EstablishIdHubController(clearCache bool, dbPath string, logger *zLogger.ZLogger) (*IdHubController, error) {
	cache, err := EstablishIdHubCache(dbPath)
	if err != nil {
		return nil, err
	}
	if clearCache {
		err := cache.Clear()
		if err != nil {
			return nil, err
		}
	}
	return &IdHubController{
		cache:        cache,
		useElastic:   false,
		elastic:      nil,
		lobidBaseUrl: "https://lobid.org/gnd/%s",
		logger:       *logger,
		client:       &http.Client{},
	}, nil
}

func EstablishIdHubControllerWithElasticCache(elastic *Elastic, logger *zLogger.ZLogger) *IdHubController {
	return &IdHubController{
		cache:        nil,
		useElastic:   true,
		elastic:      elastic,
		lobidBaseUrl: "https://lobid.org/gnd/%s.json",
		logger:       *logger,
		client:       &http.Client{},
	}
}

func (c IdHubController) GetMatch(identifier string, authority string) (*IdHubEntry, error) {
	var entry *IdHubEntry
	var err error
	if c.useElastic {
		entry, err = c.elastic.GetHubEntry(identifier)
	} else {
		entry, err = c.cache.Get(identifier)
	}
	if err == nil {
		return entry, nil
	}
	if errors.Is(err, NoConnection) {
		return nil, err
	}
	url := fmt.Sprintf(c.lobidBaseUrl, identifier)
	c.logger.Info().Msgf("GET %s", url)

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	request.Header.Set("Accept", "application/json")

	response, err := c.client.Do(request)
	if err != nil {
		return nil, err
	}

	switch response.StatusCode {
	case http.StatusOK:
		wikidataId, wikidataIdErr := c.parseResponse(response)
		if wikidataIdErr != nil {
			return nil, wikidataIdErr
		}
		newEntry := &IdHubEntry{
			GNDIdentifier:      identifier,
			WikidataIdentifier: wikidataId,
		}
		var cacheError error
		if c.useElastic {
			cacheError = c.elastic.IndexHubEntry(newEntry)
		} else {
			cacheError = c.cache.Set(newEntry)
		}
		if cacheError != nil {
			return nil, cacheError
		}
		return newEntry, nil
	case http.StatusNotFound:
		return nil, errors.New("resource not found on lobid: " + identifier)
	default:
		return nil, errors.New("unexpected status code for lobid: " + response.Status)
	}
}

func (c IdHubController) parseResponse(response *http.Response) (string, error) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(response.Body)
	content, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	lobidEntry, parseError := ParseLobidResponse(content)
	if parseError != nil {
		return "", parseError
	}
	wikidataId, wikidataIdError := lobidEntry.GetWikidataId()
	if wikidataIdError != nil {
		return "", wikidataIdError
	}
	return wikidataId, nil
}
