package shared

import (
	"bytes"
	"emperror.dev/errors"
	"encoding/json"
	"io"
)

// Resource is a generic representation of a Wikidata resource.
type Resource struct {
	ID           string              `json:"id,omitempty"`
	Labels       map[string]string   `json:"labels,omitempty"`
	Descriptions map[string]string   `json:"descriptions,omitempty"`
	Data         map[string][]*Data  `json:"data,omitempty"`
	SiteLinks    map[string]SiteLink `json:"siteLinks,omitempty"`
	LastModified string              `json:"lastModified,omitempty"`
	OriginId     string              `json:"originId,omitempty"`
	ErrorType    string              `json:"errorType,omitempty"`
	Error        string              `json:"error,omitempty"`
}

func NewErrorResource(origin, errorType, error string) *Resource {
	return &Resource{
		OriginId:  origin,
		ErrorType: errorType,
		Error:     error,
	}
}

type Data struct {
	Simple          string           `json:"simple,omitempty"`
	Time            *Time            `json:"time,omitempty"`
	Quantity        *Quantity        `json:"quantity,omitempty"`
	GlobeCoordinate *GlobeCoordinate `json:"globeCoordinate,omitempty"`
	MonoLingualText *MonoLingualText `json:"monoLingualText,omitempty"`
}

type GlobeCoordinate struct {
	Latitude  string  `json:"latitude,omitempty"`
	Longitude string  `json:"longitude,omitempty"`
	Globe     string  `json:"globe,omitempty"`
	Precision float64 `json:"precision,omitempty"`
}

type Time struct {
	Time          string  `json:"time,omitempty"`
	TimeZone      string  `json:"timezone,omitempty"`
	Before        string  `json:"before,omitempty"`
	After         string  `json:"after,omitempty"`
	Precision     float64 `json:"precision,omitempty"`
	CalendarModel string  `json:"calendarmodel,omitempty"`
}

type Quantity struct {
	Amount     string `json:"amount,omitempty"`
	Unit       string `json:"unit,omitempty"`
	Upperbound string `json:"upperbound,omitempty"`
	Lowerbound string `json:"lowerbound,omitempty"`
}

type MonoLingualText struct {
	Text     string `json:"value,omitempty"`
	Language string `json:"language,omitempty"`
}

// ToReader converts the Resource to an io.Reader.
func (r Resource) ToReader() (io.Reader, error) {
	result, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(result), nil
}

type ResourceNotFoundError struct {
	Identifier string
}

func (e ResourceNotFoundError) Error() string {
	return "resource not found: " + e.Identifier
}

func IsResourceNotFoundError(err error) bool {
	var resourceNotFoundError ResourceNotFoundError
	ok := errors.As(err, &resourceNotFoundError)
	return ok
}

type WikidataResourceFilter struct {
	Languages        []string `json:"languages,omitempty"`
	Properties       []string `json:"properties,omitempty"`
	IncludeSiteLinks bool     `json:"includeSiteLinks,omitempty"`
}

// ToReader converts the Resource to an io.Reader.
func (r *WikidataResourceFilter) ToReader() (io.Reader, error) {
	result, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(result), nil
}
