package shared

import (
	"crypto/tls"
	"crypto/x509"
	"github.com/je4/utils/v2/pkg/zLogger"
	"os"
)

func LoadCertificateConfig(tlsCertPath string, tlsKeyPath string) (*tls.Config, error) {
	if tlsCertPath == "" || tlsKeyPath == "" {
		return nil, nil
	}
	certificate, err := tls.LoadX509KeyPair(tlsCertPath, tlsKeyPath)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates: []tls.Certificate{certificate},
	}, nil
}

func LoadCaCertificate(tlsCaCertPath string, logger zLogger.ZLogger) []byte {
	if tlsCaCertPath == "" {
		logger.Warn().Msg("No tls ca certificate provided.")
		return nil
	}
	caCert, err := os.ReadFile(tlsCaCertPath)
	if err != nil {
		logger.Fatal().Msgf("Cannot load tls ca certificate: %v", err)
	}
	return caCert
}

func LoadCaCertConfig(tlsCaCertPath string) (*tls.Config, error) {
	caCert, err := os.ReadFile(tlsCaCertPath)
	if err != nil {
		return nil, err
	}

	caCertPool, err := x509.SystemCertPool()
	if err != nil {
		caCertPool = x509.NewCertPool()
	}
	if caCert != nil {
		caCertPool.AppendCertsFromPEM(caCert)
	}
	return &tls.Config{
		RootCAs: caCertPool,
	}, nil
}

func LoadLogstashCert(tlsCaCertPath, tlsCertPath, tlsKeyPath string) (*tls.Config, error) {
	if tlsCertPath == "" || tlsKeyPath == "" {
		return nil, nil
	}
	certificate, err := tls.LoadX509KeyPair(tlsCertPath, tlsKeyPath)
	if err != nil {
		return nil, err
	}

	caCert, err := os.ReadFile(tlsCaCertPath)
	if err != nil {
		return nil, err
	}

	caCertPool, err := x509.SystemCertPool()
	if err != nil {
		caCertPool = x509.NewCertPool()
	}
	if caCert != nil {
		caCertPool.AppendCertsFromPEM(caCert)
	}
	return &tls.Config{
		Certificates: []tls.Certificate{certificate},
		RootCAs:      caCertPool,
	}, nil
}
