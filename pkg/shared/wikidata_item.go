package shared

import "fmt"

type WikidataItem struct {
	ID           string                 `json:"id" example:"Q42"`
	Type         string                 `json:"type" example:"item"`
	Labels       map[string]string      `json:"labels"`
	Descriptions map[string]string      `json:"descriptions"`
	Statements   map[string][]Statement `json:"statements"`
	SiteLinks    map[string]SiteLink    `json:"siteLinks"`
}

type Statement struct {
	ID       string            `json:"id" example:"P31"`
	Rank     string            `json:"rank" example:"normal"`
	Property StatementProperty `json:"property"`
	Value    StatementValue    `json:"value"`
}

type StatementProperty struct {
	ID       string `json:"id" example:"P106"`
	DataType string `json:"data_type" example:"wikibase-item"`
}

type StatementValue struct {
	Type    string      `json:"type" example:"value"`
	Content interface{} `json:"content"`
}

func (v StatementValue) GetTime() *Time {
	content, ok := v.Content.(map[string]interface{})
	if !ok {
		return nil
	}

	timeStr, timeOK := content["time"].(string)
	timeZone, timeZoneOK := content["timezone"].(string)
	before, beforeOK := content["before"].(string)
	after, afterOK := content["after"].(string)
	precision, precisionOK := content["precision"].(float64)
	calendarModel, calendarModelOK := content["calendarmodel"].(string)

	return &Time{
		Time:          checkOk(timeStr, timeOK),
		TimeZone:      checkOk(timeZone, timeZoneOK),
		Before:        checkOk(before, beforeOK),
		After:         checkOk(after, afterOK),
		Precision:     checkOkPrecision(precision, precisionOK),
		CalendarModel: checkOk(calendarModel, calendarModelOK),
	}
}

func checkOk(value string, ok bool) string {
	if ok {
		return value
	} else {
		return ""
	}
}

func checkOkPrecision(value float64, ok bool) float64 {
	if ok {
		return value

	} else {
		return 0
	}
}

func (v StatementValue) GetQuantity() *Quantity {
	content, ok := v.Content.(map[string]interface{})
	if !ok {
		return nil
	}

	amount, amountOk := content["amount"].(string)
	unit, unitOk := content["unit"].(string)
	upperbound, upperboundOK := content["upperbound"].(string)
	lowerbound, lowerboundOK := content["lowerbound"].(string)

	return &Quantity{
		Amount:     checkOk(amount, amountOk),
		Unit:       checkOk(unit, unitOk),
		Upperbound: checkOk(upperbound, upperboundOK),
		Lowerbound: checkOk(lowerbound, lowerboundOK),
	}
}

func (v StatementValue) GetMonolingualText() *MonoLingualText {
	content, ok := v.Content.(map[string]interface{})
	if !ok {
		return nil
	}

	text, textOk := content["text"].(string)
	lang, langOk := content["language"].(string)
	return &MonoLingualText{
		Text:     checkOk(text, textOk),
		Language: checkOk(lang, langOk),
	}
}

func (v StatementValue) GetGlobeCoordinate() *GlobeCoordinate {
	content, ok := v.Content.(map[string]interface{})
	if !ok {
		return nil
	}
	lat, latOk := content["latitude"].(float64)
	lon, lonOk := content["longitude"].(float64)
	globe, globeOk := content["globe"].(string)
	prec, precOk := content["precision"].(float64)
	return &GlobeCoordinate{
		Latitude:  checkOk(fmt.Sprintf("%f", lat), latOk),
		Longitude: checkOk(fmt.Sprintf("%f", lon), lonOk),
		Globe:     checkOk(globe, globeOk),
		Precision: checkOkPrecision(prec, precOk),
	}
}

type SiteLink struct {
	Title  string   `json:"title"`
	Badges []string `json:"badges"`
	Url    string   `json:"url"`
}
