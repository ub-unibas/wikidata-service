package shared

import (
	"testing"
)

func TestWikidataClient(t *testing.T) {
	logger, errLogger := DefineLogger("", "debug")
	if errLogger != nil {
		t.Errorf("Error defining logger: %v", errLogger)
		return
	}
	client := NewWikidataRestClient(
		[]string{"en", "de", "fr", "it", "rm"},
		[]string{"P31", "P18"},
		"type,labels,descriptions,statements,sitelinks",
		NewServiceHttpClient(nil),
		&logger,
	)

	resource, err := client.Get("Q42")
	if err != nil {
		t.Errorf("Error getting resource: %v", err)
		return
	}

	if resource == nil {
		t.Errorf("Resource is nil")
		return
	}

	if resource.ID != "Q42" {
		t.Errorf("Expected ID: %s, Got: %s", "Q42", resource.ID)
	}

	if resource.Labels["en"] != "Douglas Adams" {
		t.Errorf("Expected Label: %s, Got: %s", "Douglas Adams", resource.Labels["en"])
	}

	if resource.Descriptions["en"] != "English author and humourist (1952–2001)" {
		t.Errorf("Expected Description: %s, Got: %s", "English author and humourist (1952–2001)", resource.Descriptions["en"])
	}

	if resource.Data["P31"] == nil {
		t.Errorf("Expected Statement for P31")
	}

	if resource.Data["P18"] == nil {
		t.Errorf("Expected Statement for P18")
	}

	if resource.Data["P31"][0].Simple != "Q5" {
		t.Errorf("Expected Statement for P31: %s, Got: %s", "Q5", resource.Data["P31"][0].Simple)
	}

	if resource.Data["P18"][0].Simple != "https://commons.wikimedia.org/wiki/File:Douglas_adams_portrait_cropped.jpg" {
		t.Errorf("Expected Statement for P18: %s, Got: %s", "https://commons.wikimedia.org/wiki/File:Douglas_adams_portrait_cropped.jpg", resource.Data["P18"][0].Simple)
	}
}

func TestAThousandEntities(t *testing.T) {
	logger, errLogger := DefineLogger("", "debug")
	if errLogger != nil {
		t.Errorf("Error defining logger: %v", errLogger)
		return
	}
	client := NewWikidataRestClient(
		[]string{"en", "de", "fr", "it", "rm"},
		[]string{
			"P18",   /* https://www.wikidata.org/wiki/Property:P18 image */
			"P31",   /* https://www.wikidata.org/wiki/Property:P31 instance of */
			"P214",  /* https://www.wikidata.org/wiki/Property:P214 VIAF ID */
			"P227",  /* https://www.wikidata.org/wiki/Property:P227 GND ID */
			"P244",  /* https://www.wikidata.org/wiki/Property:P244 Library of Congress authority ID */
			"P268",  /* https://www.wikidata.org/wiki/Property:P268 BnF ID */
			"P269",  /* https://www.wikidata.org/wiki/Property:P269 IdRef ID */
			"P349",  /* https://www.wikidata.org/wiki/Property:P349 NDL Auth ID */
			"P373",  /* https://www.wikidata.org/wiki/Property:P373 Commons category */
			"P496",  /* https://www.wikidata.org/wiki/Property:P496 ORCID iD */
			"P691",  /* https://www.wikidata.org/wiki/Property:P691 NKCR AUT ID */
			"P1006", /* https://www.wikidata.org/wiki/Property:P1006 NTA ID */
			"P1014", /* https://www.wikidata.org/wiki/Property:P1014 AAT ID */
			"P1015", /* https://www.wikidata.org/wiki/Property:P1015 BIBSYS ID */
			"P1025", /* https://www.wikidata.org/wiki/Property:P1025 BNE ID */
			"P1040", /* https://www.wikidata.org/wiki/Property:P1040 ISNI */
			"P1051", /* https://www.wikidata.org/wiki/Property:P1051 CANTIC ID */
			"P1055", /* https://www.wikidata.org/wiki/Property:P1055 NUKAT ID */
			"P1065", /* https://www.wikidata.org/wiki/Property:P1065 BAV ID */
		},
		"type,labels,descriptions,statements",
		NewServiceHttpClient(nil),
		&logger,
	)
	for _, q := range []string{
		"Q42",    /* https://www.wikidata.org/wiki/Q42 Douglas Adams */
		"Q1345",  /* https://www.wikidata.org/wiki/Q1345 Philadelphia */
		"Q50012", /* https://www.wikidata.org/wiki/Q50012 Josefine Preuß */
		"Q36322", /* https://www.wikidata.org/wiki/Q36322 Jane Austen */
	} {
		resource, err := client.Get(q)

		if err != nil {
			if err.Error() == "resource not found: "+q {
				continue
			}
			t.Errorf("Error getting resource: %v", err)
			return
		}

		if resource == nil {
			t.Errorf("Resource is nil")
			return
		}

		if resource.ID != q {
			t.Errorf("Expected ID: %s, Got: %s", q, resource.ID)
		}
	}
}

func TestGetLastModified(t *testing.T) {
	logger, errLogger := DefineLogger("", "debug")
	if errLogger != nil {
		t.Errorf("Error defining logger: %v", errLogger)
		return
	}
	client := NewWikidataRestClient(
		[]string{"en", "de", "fr", "it", "rm"},
		[]string{},
		"type,labels,descriptions,statements,sitelinks",
		NewServiceHttpClient(nil),
		&logger,
	)

	lastModified, err := client.GetLastModified("Q42")
	if err != nil {
		t.Errorf("Failed to get last modified %v", err)
	}
	if lastModified != "" {
		t.Errorf("Failed to get correct date. Expected  got %s.", lastModified)
	}
}
