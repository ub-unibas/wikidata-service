package shared

import (
	"bytes"
	"emperror.dev/errors"
	"encoding/json"
	"github.com/dgraph-io/badger/v4"
	"io"
	"time"
)

type IdHubEntry struct {
	GNDIdentifier      string `json:"gndIdentifier"`
	WikidataIdentifier string `json:"wikidataIdentifier"`
}

var NoEntry = errors.New("No entry")
var NoConnection = errors.New("No wikidata connection on lobid")

const NoConnectionFound = "NoConnectionFound"

func (ihe *IdHubEntry) ToReader() (io.Reader, error) {
	result, err := json.Marshal(ihe)
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(result), nil
}

type IdHubCache struct {
	db *badger.DB
}

func EstablishIdHubCache(folder string) (*IdHubCache, error) {
	cache := &IdHubCache{db: nil}
	var err error
	if folder == "" {
		opt := badger.DefaultOptions("").WithInMemory(true)
		cache.db, err = badger.Open(opt)
	} else {
		cache.db, err = badger.Open(badger.DefaultOptions(folder))
		if err != nil {
			return nil, errors.Wrapf(err, "Cannot open badger in '%s'", folder)
		}
	}
	return cache, nil
}

func (cache *IdHubCache) Get(key string) (*IdHubEntry, error) {
	var data []byte
	err := cache.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(key))
		if err != nil {
			if IsNotFound(err) {
				return ResourceNotFoundError{Identifier: key}
			} else {
				return err
			}
		}
		data, err = item.ValueCopy(nil)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	if len(data) == 0 {
		return nil, NoEntry
	}
	result := string(data)
	if result == NoConnectionFound {
		return nil, NoConnection
	}
	return &IdHubEntry{
		GNDIdentifier:      key,
		WikidataIdentifier: string(data),
	}, nil
}

func (cache *IdHubCache) Set(value *IdHubEntry) error {
	err := cache.db.Update(func(txn *badger.Txn) error {
		// hard coded to 24h as this is how often lobid is updated.
		newEntry := badger.NewEntry([]byte(value.GNDIdentifier), []byte(value.WikidataIdentifier)).WithTTL(time.Hour * 24)
		err := txn.SetEntry(newEntry)
		return err
	})
	return err
}

func (cache *IdHubCache) Clear() error {
	err := cache.db.DropAll()
	return err
}

func (cache *IdHubCache) Close() {
	err := cache.db.Close()
	if err != nil {
		return
	}
}

func IsNotFound(err error) bool {
	return errors.Is(err, badger.ErrKeyNotFound)
}
