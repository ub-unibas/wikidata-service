package shared

import (
	"emperror.dev/errors"
	"encoding/json"
	"strings"
)

const WikidataCollectionAbbr = "WIKIDATA"

type LobidEntry struct {
	GndIdentifier string   `json:"gndIdentifier"`
	Id            string   `json:"id"`
	SameAs        []SameAs `json:"sameAs"`
}

type SameAs struct {
	Id         string     `json:"id"`
	Collection Collection `json:"collection"`
}

type Collection struct {
	Id        string `json:"id"`
	Abbr      string `json:"abbr"`
	Publisher string `json:"publisher"`
	Icon      string `json:"icon"`
	Name      string `json:"name"`
}

func ParseLobidResponse(data []byte) (*LobidEntry, error) {
	var result = &LobidEntry{}
	err := json.Unmarshal(data, result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (l LobidEntry) GetWikidataId() (string, error) {
	for _, entry := range l.SameAs {
		if entry.Collection.Abbr == WikidataCollectionAbbr {
			split := strings.SplitAfter(entry.Id, "/")
			if len(split) != 5 {
				return "", errors.Errorf("invalid wikidata uri: %s", entry.Id)
			}
			return split[4], nil
		}
	}
	return "", NoConnection
}
