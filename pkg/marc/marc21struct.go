package marc

import (
	"time"
)

/*
   MARC-8 vs. UTF-8 encoding
       * leader.CharacterCodingScheme == "a" is UCS/Unicode
       * https://www.loc.gov/marc/specifications/speccharucs.html
       * https://www.loc.gov/marc/specifications/codetables.xml
       * https://lcweb2.loc.gov/diglib/codetables/eacc2uni.txt
*/

/*
https://www.loc.gov/marc/specifications/specrecstruc.html
*/

const (
	delimiter        = byte(0x1f)
	fieldTerminator  = byte(0x1e)
	recordTerminator = byte(0x1d)
	leaderLen        = 24
	maxRecordSize    = 99999
)

// Leader is for containing the text string of the MARC record Leader
type Leader struct {
	Text string `xml:",chardata" json:"text"`
}

type Date time.Time
type DateTime time.Time
type Controlfields []*Controlfield

// Record is for containing a MARC record
type Record struct {
	Leader        Leader        `xml:"leader" json:"LDR"`
	Controlfields Controlfields `xml:"controlfield" json:"controlfield"`
	Datafields    []*Datafield  `xml:"datafield" json:"datafield"`
}

// Controlfield contains a controlfield entry
type Controlfield struct {
	Tag  string `xml:"tag,attr" json:"tag"`
	Text string `xml:",chardata" json:"text"`
	Type string `xml:"-" json:"-"`
}

// Datafield contains a datafield entry
type Datafield struct {
	Tag       string      `xml:"tag,attr" json:"tag"`
	Ind1      string      `xml:"ind1,attr" json:"ind1,omitempty"`
	Ind2      string      `xml:"ind2,attr" json:"ind2,omitempty"`
	Subfields []*Subfield `xml:"subfield" json:"subfield,omitempty"`
}

// Subfield contains a subfield entry
type Subfield struct {
	Code      string `xml:"code,attr" json:"code"`
	Text      string `xml:",chardata" json:"text"`
	Datafield *Datafield
}
type QueryStruct struct {
	Name          string          `json:"name"`
	Field         *Datafield      `json:"field"`
	Datafields    []*Datafield    `json:"datafield"`
	Controlfields []*Controlfield `json:"controlfield"`
	Leader        string          `json:"LDR"`
	//	tagRef        map[string]int  `json:"-"`
}

type QueryStructMARCIJ struct {
	Name   string   `json:"name"`
	Field  *IJField `json:"field"`
	Object *MARCIJ  `json:"object"`
}
